; ModuleID = 'benchmarks/core/core_list_join.c'
source_filename = "benchmarks/core/core_list_join.c"
target datalayout = "e-m:e-i8:8:32-i16:16:32-i64:64-i128:128-n32:64-S128"
target triple = "aarch64-unknown-linux-gnu"

%struct.RESULTS_S = type { i16, i16, i16, [4 x ptr], i64, i64, i64, ptr, %struct.MAT_PARAMS_S, i16, i16, i16, i16, i16, %struct.CORE_PORTABLE_S }
%struct.MAT_PARAMS_S = type { i32, ptr, ptr, ptr }
%struct.CORE_PORTABLE_S = type { i8 }
%struct.list_data_s = type { i16, i16 }
%struct.list_head_s = type { ptr, ptr }

; Function Attrs: noinline nounwind  uwtable
define dso_local i16 @calc_func(ptr noundef %0, ptr noundef %1) #0 {
  %3 = alloca i16, align 2
  %4 = alloca ptr, align 8
  %5 = alloca ptr, align 8
  %6 = alloca i16, align 2
  %7 = alloca i16, align 2
  %8 = alloca i8, align 1
  %9 = alloca i16, align 2
  %10 = alloca i16, align 2
  store ptr %0, ptr %4, align 8
  store ptr %1, ptr %5, align 8
  %11 = load ptr, ptr %4, align 8
  %12 = load i16, ptr %11, align 2
  store i16 %12, ptr %6, align 2
  %13 = load i16, ptr %6, align 2
  %14 = sext i16 %13 to i32
  %15 = ashr i32 %14, 7
  %16 = and i32 %15, 1
  %17 = trunc i32 %16 to i8
  store i8 %17, ptr %8, align 1
  %18 = load i8, ptr %8, align 1
  %19 = icmp ne i8 %18, 0
  br i1 %19, label %20, label %25

20:                                               ; preds = %2
  %21 = load i16, ptr %6, align 2
  %22 = sext i16 %21 to i32
  %23 = and i32 %22, 127
  %24 = trunc i32 %23 to i16
  store i16 %24, ptr %3, align 2
  br label %120

25:                                               ; preds = %2
  %26 = load i16, ptr %6, align 2
  %27 = sext i16 %26 to i32
  %28 = and i32 %27, 7
  %29 = trunc i32 %28 to i16
  store i16 %29, ptr %9, align 2
  %30 = load i16, ptr %6, align 2
  %31 = sext i16 %30 to i32
  %32 = ashr i32 %31, 3
  %33 = and i32 %32, 15
  %34 = trunc i32 %33 to i16
  store i16 %34, ptr %10, align 2
  %35 = load i16, ptr %10, align 2
  %36 = sext i16 %35 to i32
  %37 = shl i32 %36, 4
  %38 = load i16, ptr %10, align 2
  %39 = sext i16 %38 to i32
  %40 = or i32 %39, %37
  %41 = trunc i32 %40 to i16
  store i16 %41, ptr %10, align 2
  %42 = load i16, ptr %9, align 2
  %43 = sext i16 %42 to i32
  switch i32 %43, label %96 [
    i32 0, label %44
    i32 1, label %78
  ]

44:                                               ; preds = %25
  %45 = load i16, ptr %10, align 2
  %46 = sext i16 %45 to i32
  %47 = icmp slt i32 %46, 34
  br i1 %47, label %48, label %49

48:                                               ; preds = %44
  store i16 34, ptr %10, align 2
  br label %49

49:                                               ; preds = %48, %44
  %50 = load ptr, ptr %5, align 8
  %51 = getelementptr inbounds %struct.RESULTS_S, ptr %50, i32 0, i32 4
  %52 = load i64, ptr %51, align 8
  %53 = load ptr, ptr %5, align 8
  %54 = getelementptr inbounds %struct.RESULTS_S, ptr %53, i32 0, i32 3
  %55 = getelementptr inbounds [4 x ptr], ptr %54, i64 0, i64 3
  %56 = load ptr, ptr %55, align 8
  %57 = load ptr, ptr %5, align 8
  %58 = getelementptr inbounds %struct.RESULTS_S, ptr %57, i32 0, i32 0
  %59 = load i16, ptr %58, align 8
  %60 = load ptr, ptr %5, align 8
  %61 = getelementptr inbounds %struct.RESULTS_S, ptr %60, i32 0, i32 1
  %62 = load i16, ptr %61, align 2
  %63 = load i16, ptr %10, align 2
  %64 = load ptr, ptr %5, align 8
  %65 = getelementptr inbounds %struct.RESULTS_S, ptr %64, i32 0, i32 9
  %66 = load i16, ptr %65, align 8
  %67 = call i16 @core_bench_state(i64 noundef %52, ptr noundef %56, i16 noundef %59, i16 noundef %62, i16 noundef %63, i16 noundef %66)
  store i16 %67, ptr %7, align 2
  %68 = load ptr, ptr %5, align 8
  %69 = getelementptr inbounds %struct.RESULTS_S, ptr %68, i32 0, i32 12
  %70 = load i16, ptr %69, align 2
  %71 = zext i16 %70 to i32
  %72 = icmp eq i32 %71, 0
  br i1 %72, label %73, label %77

73:                                               ; preds = %49
  %74 = load i16, ptr %7, align 2
  %75 = load ptr, ptr %5, align 8
  %76 = getelementptr inbounds %struct.RESULTS_S, ptr %75, i32 0, i32 12
  store i16 %74, ptr %76, align 2
  br label %77

77:                                               ; preds = %73, %49
  br label %98

78:                                               ; preds = %25
  %79 = load ptr, ptr %5, align 8
  %80 = getelementptr inbounds %struct.RESULTS_S, ptr %79, i32 0, i32 8
  %81 = load i16, ptr %10, align 2
  %82 = load ptr, ptr %5, align 8
  %83 = getelementptr inbounds %struct.RESULTS_S, ptr %82, i32 0, i32 9
  %84 = load i16, ptr %83, align 8
  %85 = call i16 @core_bench_matrix(ptr noundef %80, i16 noundef %81, i16 noundef %84)
  store i16 %85, ptr %7, align 2
  %86 = load ptr, ptr %5, align 8
  %87 = getelementptr inbounds %struct.RESULTS_S, ptr %86, i32 0, i32 11
  %88 = load i16, ptr %87, align 4
  %89 = zext i16 %88 to i32
  %90 = icmp eq i32 %89, 0
  br i1 %90, label %91, label %95

91:                                               ; preds = %78
  %92 = load i16, ptr %7, align 2
  %93 = load ptr, ptr %5, align 8
  %94 = getelementptr inbounds %struct.RESULTS_S, ptr %93, i32 0, i32 11
  store i16 %92, ptr %94, align 4
  br label %95

95:                                               ; preds = %91, %78
  br label %98

96:                                               ; preds = %25
  %97 = load i16, ptr %6, align 2
  store i16 %97, ptr %7, align 2
  br label %98

98:                                               ; preds = %96, %95, %77
  %99 = load i16, ptr %7, align 2
  %100 = load ptr, ptr %5, align 8
  %101 = getelementptr inbounds %struct.RESULTS_S, ptr %100, i32 0, i32 9
  %102 = load i16, ptr %101, align 8
  %103 = call i16 @crcu16(i16 noundef %99, i16 noundef %102)
  %104 = load ptr, ptr %5, align 8
  %105 = getelementptr inbounds %struct.RESULTS_S, ptr %104, i32 0, i32 9
  store i16 %103, ptr %105, align 8
  %106 = load i16, ptr %7, align 2
  %107 = sext i16 %106 to i32
  %108 = and i32 %107, 127
  %109 = trunc i32 %108 to i16
  store i16 %109, ptr %7, align 2
  %110 = load i16, ptr %6, align 2
  %111 = sext i16 %110 to i32
  %112 = and i32 %111, 65280
  %113 = or i32 %112, 128
  %114 = load i16, ptr %7, align 2
  %115 = sext i16 %114 to i32
  %116 = or i32 %113, %115
  %117 = trunc i32 %116 to i16
  %118 = load ptr, ptr %4, align 8
  store i16 %117, ptr %118, align 2
  %119 = load i16, ptr %7, align 2
  store i16 %119, ptr %3, align 2
  br label %120

120:                                              ; preds = %98, %20
  %121 = load i16, ptr %3, align 2
  ret i16 %121
}

declare i16 @core_bench_state(i64 noundef, ptr noundef, i16 noundef, i16 noundef, i16 noundef, i16 noundef) #1

declare i16 @core_bench_matrix(ptr noundef, i16 noundef, i16 noundef) #1

declare i16 @crcu16(i16 noundef, i16 noundef) #1

; Function Attrs: noinline nounwind  uwtable
define dso_local i64 @cmp_complex(ptr noundef %0, ptr noundef %1, ptr noundef %2) #0 {
  %4 = alloca ptr, align 8
  %5 = alloca ptr, align 8
  %6 = alloca ptr, align 8
  %7 = alloca i16, align 2
  %8 = alloca i16, align 2
  store ptr %0, ptr %4, align 8
  store ptr %1, ptr %5, align 8
  store ptr %2, ptr %6, align 8
  %9 = load ptr, ptr %4, align 8
  %10 = getelementptr inbounds %struct.list_data_s, ptr %9, i32 0, i32 0
  %11 = load ptr, ptr %6, align 8
  %12 = call i16 @calc_func(ptr noundef %10, ptr noundef %11)
  store i16 %12, ptr %7, align 2
  %13 = load ptr, ptr %5, align 8
  %14 = getelementptr inbounds %struct.list_data_s, ptr %13, i32 0, i32 0
  %15 = load ptr, ptr %6, align 8
  %16 = call i16 @calc_func(ptr noundef %14, ptr noundef %15)
  store i16 %16, ptr %8, align 2
  %17 = load i16, ptr %7, align 2
  %18 = sext i16 %17 to i32
  %19 = load i16, ptr %8, align 2
  %20 = sext i16 %19 to i32
  %21 = sub nsw i32 %18, %20
  %22 = sext i32 %21 to i64
  ret i64 %22
}

; Function Attrs: noinline nounwind  uwtable
define dso_local i64 @cmp_idx(ptr noundef %0, ptr noundef %1, ptr noundef %2) #0 {
  %4 = alloca ptr, align 8
  %5 = alloca ptr, align 8
  %6 = alloca ptr, align 8
  store ptr %0, ptr %4, align 8
  store ptr %1, ptr %5, align 8
  store ptr %2, ptr %6, align 8
  %7 = load ptr, ptr %6, align 8
  %8 = icmp eq ptr %7, null
  br i1 %8, label %9, label %40

9:                                                ; preds = %3
  %10 = load ptr, ptr %4, align 8
  %11 = getelementptr inbounds %struct.list_data_s, ptr %10, i32 0, i32 0
  %12 = load i16, ptr %11, align 2
  %13 = sext i16 %12 to i32
  %14 = and i32 %13, 65280
  %15 = load ptr, ptr %4, align 8
  %16 = getelementptr inbounds %struct.list_data_s, ptr %15, i32 0, i32 0
  %17 = load i16, ptr %16, align 2
  %18 = sext i16 %17 to i32
  %19 = ashr i32 %18, 8
  %20 = and i32 255, %19
  %21 = or i32 %14, %20
  %22 = trunc i32 %21 to i16
  %23 = load ptr, ptr %4, align 8
  %24 = getelementptr inbounds %struct.list_data_s, ptr %23, i32 0, i32 0
  store i16 %22, ptr %24, align 2
  %25 = load ptr, ptr %5, align 8
  %26 = getelementptr inbounds %struct.list_data_s, ptr %25, i32 0, i32 0
  %27 = load i16, ptr %26, align 2
  %28 = sext i16 %27 to i32
  %29 = and i32 %28, 65280
  %30 = load ptr, ptr %5, align 8
  %31 = getelementptr inbounds %struct.list_data_s, ptr %30, i32 0, i32 0
  %32 = load i16, ptr %31, align 2
  %33 = sext i16 %32 to i32
  %34 = ashr i32 %33, 8
  %35 = and i32 255, %34
  %36 = or i32 %29, %35
  %37 = trunc i32 %36 to i16
  %38 = load ptr, ptr %5, align 8
  %39 = getelementptr inbounds %struct.list_data_s, ptr %38, i32 0, i32 0
  store i16 %37, ptr %39, align 2
  br label %40

40:                                               ; preds = %9, %3
  %41 = load ptr, ptr %4, align 8
  %42 = getelementptr inbounds %struct.list_data_s, ptr %41, i32 0, i32 1
  %43 = load i16, ptr %42, align 2
  %44 = sext i16 %43 to i32
  %45 = load ptr, ptr %5, align 8
  %46 = getelementptr inbounds %struct.list_data_s, ptr %45, i32 0, i32 1
  %47 = load i16, ptr %46, align 2
  %48 = sext i16 %47 to i32
  %49 = sub nsw i32 %44, %48
  %50 = sext i32 %49 to i64
  ret i64 %50
}

; Function Attrs: noinline nounwind  uwtable
define dso_local void @copy_info(ptr noundef %0, ptr noundef %1) #0 {
  %3 = alloca ptr, align 8
  %4 = alloca ptr, align 8
  store ptr %0, ptr %3, align 8
  store ptr %1, ptr %4, align 8
  %5 = load ptr, ptr %4, align 8
  %6 = getelementptr inbounds %struct.list_data_s, ptr %5, i32 0, i32 0
  %7 = load i16, ptr %6, align 2
  %8 = load ptr, ptr %3, align 8
  %9 = getelementptr inbounds %struct.list_data_s, ptr %8, i32 0, i32 0
  store i16 %7, ptr %9, align 2
  %10 = load ptr, ptr %4, align 8
  %11 = getelementptr inbounds %struct.list_data_s, ptr %10, i32 0, i32 1
  %12 = load i16, ptr %11, align 2
  %13 = load ptr, ptr %3, align 8
  %14 = getelementptr inbounds %struct.list_data_s, ptr %13, i32 0, i32 1
  store i16 %12, ptr %14, align 2
  ret void
}

; Function Attrs: noinline nounwind  uwtable
define dso_local i16 @core_bench_list(ptr noundef %0, i16 noundef %1) #0 {
  %3 = alloca ptr, align 8
  %4 = alloca i16, align 2
  %5 = alloca i16, align 2
  %6 = alloca i16, align 2
  %7 = alloca i16, align 2
  %8 = alloca ptr, align 8
  %9 = alloca i16, align 2
  %10 = alloca ptr, align 8
  %11 = alloca ptr, align 8
  %12 = alloca ptr, align 8
  %13 = alloca %struct.list_data_s, align 2
  %14 = alloca i16, align 2
  store ptr %0, ptr %3, align 8
  store i16 %1, ptr %4, align 2
  store i16 0, ptr %5, align 2
  store i16 0, ptr %6, align 2
  store i16 0, ptr %7, align 2
  %15 = load ptr, ptr %3, align 8
  %16 = getelementptr inbounds %struct.RESULTS_S, ptr %15, i32 0, i32 7
  %17 = load ptr, ptr %16, align 8
  store ptr %17, ptr %8, align 8
  %18 = load ptr, ptr %3, align 8
  %19 = getelementptr inbounds %struct.RESULTS_S, ptr %18, i32 0, i32 2
  %20 = load i16, ptr %19, align 4
  store i16 %20, ptr %9, align 2
  %21 = load i16, ptr %4, align 2
  %22 = getelementptr inbounds %struct.list_data_s, ptr %13, i32 0, i32 1
  store i16 %21, ptr %22, align 2
  store i16 0, ptr %14, align 2
  br label %23

23:                                               ; preds = %115, %2
  %24 = load i16, ptr %14, align 2
  %25 = sext i16 %24 to i32
  %26 = load i16, ptr %9, align 2
  %27 = sext i16 %26 to i32
  %28 = icmp slt i32 %25, %27
  br i1 %28, label %29, label %118

29:                                               ; preds = %23
  %30 = load i16, ptr %14, align 2
  %31 = sext i16 %30 to i32
  %32 = and i32 %31, 255
  %33 = trunc i32 %32 to i16
  %34 = getelementptr inbounds %struct.list_data_s, ptr %13, i32 0, i32 0
  store i16 %33, ptr %34, align 2
  %35 = load ptr, ptr %8, align 8
  %36 = call ptr @core_list_find(ptr noundef %35, ptr noundef %13)
  store ptr %36, ptr %10, align 8
  %37 = load ptr, ptr %8, align 8
  %38 = call ptr @core_list_reverse(ptr noundef %37)
  store ptr %38, ptr %8, align 8
  %39 = load ptr, ptr %10, align 8
  %40 = icmp eq ptr %39, null
  br i1 %40, label %41, label %58

41:                                               ; preds = %29
  %42 = load i16, ptr %7, align 2
  %43 = add i16 %42, 1
  store i16 %43, ptr %7, align 2
  %44 = load ptr, ptr %8, align 8
  %45 = getelementptr inbounds %struct.list_head_s, ptr %44, i32 0, i32 0
  %46 = load ptr, ptr %45, align 8
  %47 = getelementptr inbounds %struct.list_head_s, ptr %46, i32 0, i32 1
  %48 = load ptr, ptr %47, align 8
  %49 = getelementptr inbounds %struct.list_data_s, ptr %48, i32 0, i32 0
  %50 = load i16, ptr %49, align 2
  %51 = sext i16 %50 to i32
  %52 = ashr i32 %51, 8
  %53 = and i32 %52, 1
  %54 = load i16, ptr %5, align 2
  %55 = zext i16 %54 to i32
  %56 = add nsw i32 %55, %53
  %57 = trunc i32 %56 to i16
  store i16 %57, ptr %5, align 2
  br label %105

58:                                               ; preds = %29
  %59 = load i16, ptr %6, align 2
  %60 = add i16 %59, 1
  store i16 %60, ptr %6, align 2
  %61 = load ptr, ptr %10, align 8
  %62 = getelementptr inbounds %struct.list_head_s, ptr %61, i32 0, i32 1
  %63 = load ptr, ptr %62, align 8
  %64 = getelementptr inbounds %struct.list_data_s, ptr %63, i32 0, i32 0
  %65 = load i16, ptr %64, align 2
  %66 = sext i16 %65 to i32
  %67 = and i32 %66, 1
  %68 = icmp ne i32 %67, 0
  br i1 %68, label %69, label %82

69:                                               ; preds = %58
  %70 = load ptr, ptr %10, align 8
  %71 = getelementptr inbounds %struct.list_head_s, ptr %70, i32 0, i32 1
  %72 = load ptr, ptr %71, align 8
  %73 = getelementptr inbounds %struct.list_data_s, ptr %72, i32 0, i32 0
  %74 = load i16, ptr %73, align 2
  %75 = sext i16 %74 to i32
  %76 = ashr i32 %75, 9
  %77 = and i32 %76, 1
  %78 = load i16, ptr %5, align 2
  %79 = zext i16 %78 to i32
  %80 = add nsw i32 %79, %77
  %81 = trunc i32 %80 to i16
  store i16 %81, ptr %5, align 2
  br label %82

82:                                               ; preds = %69, %58
  %83 = load ptr, ptr %10, align 8
  %84 = getelementptr inbounds %struct.list_head_s, ptr %83, i32 0, i32 0
  %85 = load ptr, ptr %84, align 8
  %86 = icmp ne ptr %85, null
  br i1 %86, label %87, label %104

87:                                               ; preds = %82
  %88 = load ptr, ptr %10, align 8
  %89 = getelementptr inbounds %struct.list_head_s, ptr %88, i32 0, i32 0
  %90 = load ptr, ptr %89, align 8
  store ptr %90, ptr %11, align 8
  %91 = load ptr, ptr %11, align 8
  %92 = getelementptr inbounds %struct.list_head_s, ptr %91, i32 0, i32 0
  %93 = load ptr, ptr %92, align 8
  %94 = load ptr, ptr %10, align 8
  %95 = getelementptr inbounds %struct.list_head_s, ptr %94, i32 0, i32 0
  store ptr %93, ptr %95, align 8
  %96 = load ptr, ptr %8, align 8
  %97 = getelementptr inbounds %struct.list_head_s, ptr %96, i32 0, i32 0
  %98 = load ptr, ptr %97, align 8
  %99 = load ptr, ptr %11, align 8
  %100 = getelementptr inbounds %struct.list_head_s, ptr %99, i32 0, i32 0
  store ptr %98, ptr %100, align 8
  %101 = load ptr, ptr %11, align 8
  %102 = load ptr, ptr %8, align 8
  %103 = getelementptr inbounds %struct.list_head_s, ptr %102, i32 0, i32 0
  store ptr %101, ptr %103, align 8
  br label %104

104:                                              ; preds = %87, %82
  br label %105

105:                                              ; preds = %104, %41
  %106 = getelementptr inbounds %struct.list_data_s, ptr %13, i32 0, i32 1
  %107 = load i16, ptr %106, align 2
  %108 = sext i16 %107 to i32
  %109 = icmp sge i32 %108, 0
  br i1 %109, label %110, label %114

110:                                              ; preds = %105
  %111 = getelementptr inbounds %struct.list_data_s, ptr %13, i32 0, i32 1
  %112 = load i16, ptr %111, align 2
  %113 = add i16 %112, 1
  store i16 %113, ptr %111, align 2
  br label %114

114:                                              ; preds = %110, %105
  br label %115

115:                                              ; preds = %114
  %116 = load i16, ptr %14, align 2
  %117 = add i16 %116, 1
  store i16 %117, ptr %14, align 2
  br label %23, !llvm.loop !6

118:                                              ; preds = %23
  %119 = load i16, ptr %6, align 2
  %120 = zext i16 %119 to i32
  %121 = mul nsw i32 %120, 4
  %122 = load i16, ptr %7, align 2
  %123 = zext i16 %122 to i32
  %124 = sub nsw i32 %121, %123
  %125 = load i16, ptr %5, align 2
  %126 = zext i16 %125 to i32
  %127 = add nsw i32 %126, %124
  %128 = trunc i32 %127 to i16
  store i16 %128, ptr %5, align 2
  %129 = load i16, ptr %4, align 2
  %130 = sext i16 %129 to i32
  %131 = icmp sgt i32 %130, 0
  br i1 %131, label %132, label %136

132:                                              ; preds = %118
  %133 = load ptr, ptr %8, align 8
  %134 = load ptr, ptr %3, align 8
  %135 = call ptr @core_list_mergesort(ptr noundef %133, ptr noundef @cmp_complex, ptr noundef %134)
  store ptr %135, ptr %8, align 8
  br label %136

136:                                              ; preds = %132, %118
  %137 = load ptr, ptr %8, align 8
  %138 = getelementptr inbounds %struct.list_head_s, ptr %137, i32 0, i32 0
  %139 = load ptr, ptr %138, align 8
  %140 = call ptr @core_list_remove(ptr noundef %139)
  store ptr %140, ptr %12, align 8
  %141 = load ptr, ptr %8, align 8
  %142 = call ptr @core_list_find(ptr noundef %141, ptr noundef %13)
  store ptr %142, ptr %11, align 8
  %143 = load ptr, ptr %11, align 8
  %144 = icmp ne ptr %143, null
  br i1 %144, label %149, label %145

145:                                              ; preds = %136
  %146 = load ptr, ptr %8, align 8
  %147 = getelementptr inbounds %struct.list_head_s, ptr %146, i32 0, i32 0
  %148 = load ptr, ptr %147, align 8
  store ptr %148, ptr %11, align 8
  br label %149

149:                                              ; preds = %145, %136
  br label %150

150:                                              ; preds = %153, %149
  %151 = load ptr, ptr %11, align 8
  %152 = icmp ne ptr %151, null
  br i1 %152, label %153, label %164

153:                                              ; preds = %150
  %154 = load ptr, ptr %8, align 8
  %155 = getelementptr inbounds %struct.list_head_s, ptr %154, i32 0, i32 1
  %156 = load ptr, ptr %155, align 8
  %157 = getelementptr inbounds %struct.list_data_s, ptr %156, i32 0, i32 0
  %158 = load i16, ptr %157, align 2
  %159 = load i16, ptr %5, align 2
  %160 = call i16 @crc16(i16 noundef %158, i16 noundef %159)
  store i16 %160, ptr %5, align 2
  %161 = load ptr, ptr %11, align 8
  %162 = getelementptr inbounds %struct.list_head_s, ptr %161, i32 0, i32 0
  %163 = load ptr, ptr %162, align 8
  store ptr %163, ptr %11, align 8
  br label %150, !llvm.loop !8

164:                                              ; preds = %150
  %165 = load ptr, ptr %12, align 8
  %166 = load ptr, ptr %8, align 8
  %167 = getelementptr inbounds %struct.list_head_s, ptr %166, i32 0, i32 0
  %168 = load ptr, ptr %167, align 8
  %169 = call ptr @core_list_undo_remove(ptr noundef %165, ptr noundef %168)
  store ptr %169, ptr %12, align 8
  %170 = load ptr, ptr %8, align 8
  %171 = call ptr @core_list_mergesort(ptr noundef %170, ptr noundef @cmp_idx, ptr noundef null)
  store ptr %171, ptr %8, align 8
  %172 = load ptr, ptr %8, align 8
  %173 = getelementptr inbounds %struct.list_head_s, ptr %172, i32 0, i32 0
  %174 = load ptr, ptr %173, align 8
  store ptr %174, ptr %11, align 8
  br label %175

175:                                              ; preds = %178, %164
  %176 = load ptr, ptr %11, align 8
  %177 = icmp ne ptr %176, null
  br i1 %177, label %178, label %189

178:                                              ; preds = %175
  %179 = load ptr, ptr %8, align 8
  %180 = getelementptr inbounds %struct.list_head_s, ptr %179, i32 0, i32 1
  %181 = load ptr, ptr %180, align 8
  %182 = getelementptr inbounds %struct.list_data_s, ptr %181, i32 0, i32 0
  %183 = load i16, ptr %182, align 2
  %184 = load i16, ptr %5, align 2
  %185 = call i16 @crc16(i16 noundef %183, i16 noundef %184)
  store i16 %185, ptr %5, align 2
  %186 = load ptr, ptr %11, align 8
  %187 = getelementptr inbounds %struct.list_head_s, ptr %186, i32 0, i32 0
  %188 = load ptr, ptr %187, align 8
  store ptr %188, ptr %11, align 8
  br label %175, !llvm.loop !9

189:                                              ; preds = %175
  %190 = load i16, ptr %5, align 2
  ret i16 %190
}

; Function Attrs: noinline nounwind  uwtable
define dso_local ptr @core_list_find(ptr noundef %0, ptr noundef %1) #0 {
  %3 = alloca ptr, align 8
  %4 = alloca ptr, align 8
  %5 = alloca ptr, align 8
  %6 = alloca ptr, align 8
  store ptr %0, ptr %4, align 8
  store ptr %1, ptr %5, align 8
  %7 = load ptr, ptr %4, align 8
  store ptr %7, ptr %6, align 8
  %8 = load ptr, ptr %5, align 8
  %9 = getelementptr inbounds %struct.list_data_s, ptr %8, i32 0, i32 1
  %10 = load i16, ptr %9, align 2
  %11 = sext i16 %10 to i32
  %12 = icmp sge i32 %11, 0
  br i1 %12, label %13, label %37

13:                                               ; preds = %2
  br label %14

14:                                               ; preds = %31, %13
  %15 = load ptr, ptr %6, align 8
  %16 = icmp ne ptr %15, null
  br i1 %16, label %17, label %29

17:                                               ; preds = %14
  %18 = load ptr, ptr %6, align 8
  %19 = getelementptr inbounds %struct.list_head_s, ptr %18, i32 0, i32 1
  %20 = load ptr, ptr %19, align 8
  %21 = getelementptr inbounds %struct.list_data_s, ptr %20, i32 0, i32 1
  %22 = load i16, ptr %21, align 2
  %23 = sext i16 %22 to i32
  %24 = load ptr, ptr %5, align 8
  %25 = getelementptr inbounds %struct.list_data_s, ptr %24, i32 0, i32 1
  %26 = load i16, ptr %25, align 2
  %27 = sext i16 %26 to i32
  %28 = icmp ne i32 %23, %27
  br label %29

29:                                               ; preds = %17, %14
  %30 = phi i1 [ false, %14 ], [ %28, %17 ]
  br i1 %30, label %31, label %35

31:                                               ; preds = %29
  %32 = load ptr, ptr %6, align 8
  %33 = getelementptr inbounds %struct.list_head_s, ptr %32, i32 0, i32 0
  %34 = load ptr, ptr %33, align 8
  store ptr %34, ptr %6, align 8
  br label %14, !llvm.loop !10

35:                                               ; preds = %29
  %36 = load ptr, ptr %6, align 8
  store ptr %36, ptr %3, align 8
  br label %62

37:                                               ; preds = %2
  br label %38

38:                                               ; preds = %56, %37
  %39 = load ptr, ptr %6, align 8
  %40 = icmp ne ptr %39, null
  br i1 %40, label %41, label %54

41:                                               ; preds = %38
  %42 = load ptr, ptr %6, align 8
  %43 = getelementptr inbounds %struct.list_head_s, ptr %42, i32 0, i32 1
  %44 = load ptr, ptr %43, align 8
  %45 = getelementptr inbounds %struct.list_data_s, ptr %44, i32 0, i32 0
  %46 = load i16, ptr %45, align 2
  %47 = sext i16 %46 to i32
  %48 = and i32 %47, 255
  %49 = load ptr, ptr %5, align 8
  %50 = getelementptr inbounds %struct.list_data_s, ptr %49, i32 0, i32 0
  %51 = load i16, ptr %50, align 2
  %52 = sext i16 %51 to i32
  %53 = icmp ne i32 %48, %52
  br label %54

54:                                               ; preds = %41, %38
  %55 = phi i1 [ false, %38 ], [ %53, %41 ]
  br i1 %55, label %56, label %60

56:                                               ; preds = %54
  %57 = load ptr, ptr %6, align 8
  %58 = getelementptr inbounds %struct.list_head_s, ptr %57, i32 0, i32 0
  %59 = load ptr, ptr %58, align 8
  store ptr %59, ptr %6, align 8
  br label %38, !llvm.loop !11

60:                                               ; preds = %54
  %61 = load ptr, ptr %6, align 8
  store ptr %61, ptr %3, align 8
  br label %62

62:                                               ; preds = %60, %35
  %63 = load ptr, ptr %3, align 8
  ret ptr %63
}

; Function Attrs: noinline nounwind  uwtable
define dso_local ptr @core_list_reverse(ptr noundef %0) #0 {
  %2 = alloca ptr, align 8
  %3 = alloca ptr, align 8
  %4 = alloca ptr, align 8
  store ptr %0, ptr %2, align 8
  store ptr null, ptr %3, align 8
  br label %5

5:                                                ; preds = %8, %1
  %6 = load ptr, ptr %2, align 8
  %7 = icmp ne ptr %6, null
  br i1 %7, label %8, label %17

8:                                                ; preds = %5
  %9 = load ptr, ptr %2, align 8
  %10 = getelementptr inbounds %struct.list_head_s, ptr %9, i32 0, i32 0
  %11 = load ptr, ptr %10, align 8
  store ptr %11, ptr %4, align 8
  %12 = load ptr, ptr %3, align 8
  %13 = load ptr, ptr %2, align 8
  %14 = getelementptr inbounds %struct.list_head_s, ptr %13, i32 0, i32 0
  store ptr %12, ptr %14, align 8
  %15 = load ptr, ptr %2, align 8
  store ptr %15, ptr %3, align 8
  %16 = load ptr, ptr %4, align 8
  store ptr %16, ptr %2, align 8
  br label %5, !llvm.loop !12

17:                                               ; preds = %5
  %18 = load ptr, ptr %3, align 8
  ret ptr %18
}

; Function Attrs: noinline nounwind  uwtable
define dso_local ptr @core_list_mergesort(ptr noundef %0, ptr noundef %1, ptr noundef %2) #0 {
  %4 = alloca ptr, align 8
  %5 = alloca ptr, align 8
  %6 = alloca ptr, align 8
  %7 = alloca ptr, align 8
  %8 = alloca ptr, align 8
  %9 = alloca ptr, align 8
  %10 = alloca ptr, align 8
  %11 = alloca i64, align 8
  %12 = alloca i64, align 8
  %13 = alloca i64, align 8
  %14 = alloca i64, align 8
  %15 = alloca i64, align 8
  store ptr %0, ptr %4, align 8
  store ptr %1, ptr %5, align 8
  store ptr %2, ptr %6, align 8
  store i64 1, ptr %11, align 8
  br label %16

16:                                               ; preds = %3, %127
  %17 = load ptr, ptr %4, align 8
  store ptr %17, ptr %7, align 8
  store ptr null, ptr %4, align 8
  store ptr null, ptr %10, align 8
  store i64 0, ptr %12, align 8
  br label %18

18:                                               ; preds = %118, %16
  %19 = load ptr, ptr %7, align 8
  %20 = icmp ne ptr %19, null
  br i1 %20, label %21, label %120

21:                                               ; preds = %18
  %22 = load i64, ptr %12, align 8
  %23 = add nsw i64 %22, 1
  store i64 %23, ptr %12, align 8
  %24 = load ptr, ptr %7, align 8
  store ptr %24, ptr %8, align 8
  store i64 0, ptr %13, align 8
  store i64 0, ptr %15, align 8
  br label %25

25:                                               ; preds = %39, %21
  %26 = load i64, ptr %15, align 8
  %27 = load i64, ptr %11, align 8
  %28 = icmp slt i64 %26, %27
  br i1 %28, label %29, label %42

29:                                               ; preds = %25
  %30 = load i64, ptr %13, align 8
  %31 = add nsw i64 %30, 1
  store i64 %31, ptr %13, align 8
  %32 = load ptr, ptr %8, align 8
  %33 = getelementptr inbounds %struct.list_head_s, ptr %32, i32 0, i32 0
  %34 = load ptr, ptr %33, align 8
  store ptr %34, ptr %8, align 8
  %35 = load ptr, ptr %8, align 8
  %36 = icmp ne ptr %35, null
  br i1 %36, label %38, label %37

37:                                               ; preds = %29
  br label %42

38:                                               ; preds = %29
  br label %39

39:                                               ; preds = %38
  %40 = load i64, ptr %15, align 8
  %41 = add nsw i64 %40, 1
  store i64 %41, ptr %15, align 8
  br label %25, !llvm.loop !13

42:                                               ; preds = %37, %25
  %43 = load i64, ptr %11, align 8
  store i64 %43, ptr %14, align 8
  br label %44

44:                                               ; preds = %116, %42
  %45 = load i64, ptr %13, align 8
  %46 = icmp sgt i64 %45, 0
  br i1 %46, label %55, label %47

47:                                               ; preds = %44
  %48 = load i64, ptr %14, align 8
  %49 = icmp sgt i64 %48, 0
  br i1 %49, label %50, label %53

50:                                               ; preds = %47
  %51 = load ptr, ptr %8, align 8
  %52 = icmp ne ptr %51, null
  br label %53

53:                                               ; preds = %50, %47
  %54 = phi i1 [ false, %47 ], [ %52, %50 ]
  br label %55

55:                                               ; preds = %53, %44
  %56 = phi i1 [ true, %44 ], [ %54, %53 ]
  br i1 %56, label %57, label %118

57:                                               ; preds = %55
  %58 = load i64, ptr %13, align 8
  %59 = icmp eq i64 %58, 0
  br i1 %59, label %60, label %67

60:                                               ; preds = %57
  %61 = load ptr, ptr %8, align 8
  store ptr %61, ptr %9, align 8
  %62 = load ptr, ptr %8, align 8
  %63 = getelementptr inbounds %struct.list_head_s, ptr %62, i32 0, i32 0
  %64 = load ptr, ptr %63, align 8
  store ptr %64, ptr %8, align 8
  %65 = load i64, ptr %14, align 8
  %66 = add nsw i64 %65, -1
  store i64 %66, ptr %14, align 8
  br label %107

67:                                               ; preds = %57
  %68 = load i64, ptr %14, align 8
  %69 = icmp eq i64 %68, 0
  br i1 %69, label %73, label %70

70:                                               ; preds = %67
  %71 = load ptr, ptr %8, align 8
  %72 = icmp ne ptr %71, null
  br i1 %72, label %80, label %73

73:                                               ; preds = %70, %67
  %74 = load ptr, ptr %7, align 8
  store ptr %74, ptr %9, align 8
  %75 = load ptr, ptr %7, align 8
  %76 = getelementptr inbounds %struct.list_head_s, ptr %75, i32 0, i32 0
  %77 = load ptr, ptr %76, align 8
  store ptr %77, ptr %7, align 8
  %78 = load i64, ptr %13, align 8
  %79 = add nsw i64 %78, -1
  store i64 %79, ptr %13, align 8
  br label %106

80:                                               ; preds = %70
  %81 = load ptr, ptr %5, align 8
  %82 = load ptr, ptr %7, align 8
  %83 = getelementptr inbounds %struct.list_head_s, ptr %82, i32 0, i32 1
  %84 = load ptr, ptr %83, align 8
  %85 = load ptr, ptr %8, align 8
  %86 = getelementptr inbounds %struct.list_head_s, ptr %85, i32 0, i32 1
  %87 = load ptr, ptr %86, align 8
  %88 = load ptr, ptr %6, align 8
  %89 = call i64 %81(ptr noundef %84, ptr noundef %87, ptr noundef %88)
  %90 = icmp sle i64 %89, 0
  br i1 %90, label %91, label %98

91:                                               ; preds = %80
  %92 = load ptr, ptr %7, align 8
  store ptr %92, ptr %9, align 8
  %93 = load ptr, ptr %7, align 8
  %94 = getelementptr inbounds %struct.list_head_s, ptr %93, i32 0, i32 0
  %95 = load ptr, ptr %94, align 8
  store ptr %95, ptr %7, align 8
  %96 = load i64, ptr %13, align 8
  %97 = add nsw i64 %96, -1
  store i64 %97, ptr %13, align 8
  br label %105

98:                                               ; preds = %80
  %99 = load ptr, ptr %8, align 8
  store ptr %99, ptr %9, align 8
  %100 = load ptr, ptr %8, align 8
  %101 = getelementptr inbounds %struct.list_head_s, ptr %100, i32 0, i32 0
  %102 = load ptr, ptr %101, align 8
  store ptr %102, ptr %8, align 8
  %103 = load i64, ptr %14, align 8
  %104 = add nsw i64 %103, -1
  store i64 %104, ptr %14, align 8
  br label %105

105:                                              ; preds = %98, %91
  br label %106

106:                                              ; preds = %105, %73
  br label %107

107:                                              ; preds = %106, %60
  %108 = load ptr, ptr %10, align 8
  %109 = icmp ne ptr %108, null
  br i1 %109, label %110, label %114

110:                                              ; preds = %107
  %111 = load ptr, ptr %9, align 8
  %112 = load ptr, ptr %10, align 8
  %113 = getelementptr inbounds %struct.list_head_s, ptr %112, i32 0, i32 0
  store ptr %111, ptr %113, align 8
  br label %116

114:                                              ; preds = %107
  %115 = load ptr, ptr %9, align 8
  store ptr %115, ptr %4, align 8
  br label %116

116:                                              ; preds = %114, %110
  %117 = load ptr, ptr %9, align 8
  store ptr %117, ptr %10, align 8
  br label %44, !llvm.loop !14

118:                                              ; preds = %55
  %119 = load ptr, ptr %8, align 8
  store ptr %119, ptr %7, align 8
  br label %18, !llvm.loop !15

120:                                              ; preds = %18
  %121 = load ptr, ptr %10, align 8
  %122 = getelementptr inbounds %struct.list_head_s, ptr %121, i32 0, i32 0
  store ptr null, ptr %122, align 8
  %123 = load i64, ptr %12, align 8
  %124 = icmp sle i64 %123, 1
  br i1 %124, label %125, label %127

125:                                              ; preds = %120
  %126 = load ptr, ptr %4, align 8
  ret ptr %126

127:                                              ; preds = %120
  %128 = load i64, ptr %11, align 8
  %129 = mul nsw i64 %128, 2
  store i64 %129, ptr %11, align 8
  br label %16
}

; Function Attrs: noinline nounwind  uwtable
define dso_local ptr @core_list_remove(ptr noundef %0) #0 {
  %2 = alloca ptr, align 8
  %3 = alloca ptr, align 8
  %4 = alloca ptr, align 8
  store ptr %0, ptr %2, align 8
  %5 = load ptr, ptr %2, align 8
  %6 = getelementptr inbounds %struct.list_head_s, ptr %5, i32 0, i32 0
  %7 = load ptr, ptr %6, align 8
  store ptr %7, ptr %4, align 8
  %8 = load ptr, ptr %2, align 8
  %9 = getelementptr inbounds %struct.list_head_s, ptr %8, i32 0, i32 1
  %10 = load ptr, ptr %9, align 8
  store ptr %10, ptr %3, align 8
  %11 = load ptr, ptr %4, align 8
  %12 = getelementptr inbounds %struct.list_head_s, ptr %11, i32 0, i32 1
  %13 = load ptr, ptr %12, align 8
  %14 = load ptr, ptr %2, align 8
  %15 = getelementptr inbounds %struct.list_head_s, ptr %14, i32 0, i32 1
  store ptr %13, ptr %15, align 8
  %16 = load ptr, ptr %3, align 8
  %17 = load ptr, ptr %4, align 8
  %18 = getelementptr inbounds %struct.list_head_s, ptr %17, i32 0, i32 1
  store ptr %16, ptr %18, align 8
  %19 = load ptr, ptr %2, align 8
  %20 = getelementptr inbounds %struct.list_head_s, ptr %19, i32 0, i32 0
  %21 = load ptr, ptr %20, align 8
  %22 = getelementptr inbounds %struct.list_head_s, ptr %21, i32 0, i32 0
  %23 = load ptr, ptr %22, align 8
  %24 = load ptr, ptr %2, align 8
  %25 = getelementptr inbounds %struct.list_head_s, ptr %24, i32 0, i32 0
  store ptr %23, ptr %25, align 8
  %26 = load ptr, ptr %4, align 8
  %27 = getelementptr inbounds %struct.list_head_s, ptr %26, i32 0, i32 0
  store ptr null, ptr %27, align 8
  %28 = load ptr, ptr %4, align 8
  ret ptr %28
}

declare i16 @crc16(i16 noundef, i16 noundef) #1

; Function Attrs: noinline nounwind  uwtable
define dso_local ptr @core_list_undo_remove(ptr noundef %0, ptr noundef %1) #0 {
  %3 = alloca ptr, align 8
  %4 = alloca ptr, align 8
  %5 = alloca ptr, align 8
  store ptr %0, ptr %3, align 8
  store ptr %1, ptr %4, align 8
  %6 = load ptr, ptr %3, align 8
  %7 = getelementptr inbounds %struct.list_head_s, ptr %6, i32 0, i32 1
  %8 = load ptr, ptr %7, align 8
  store ptr %8, ptr %5, align 8
  %9 = load ptr, ptr %4, align 8
  %10 = getelementptr inbounds %struct.list_head_s, ptr %9, i32 0, i32 1
  %11 = load ptr, ptr %10, align 8
  %12 = load ptr, ptr %3, align 8
  %13 = getelementptr inbounds %struct.list_head_s, ptr %12, i32 0, i32 1
  store ptr %11, ptr %13, align 8
  %14 = load ptr, ptr %5, align 8
  %15 = load ptr, ptr %4, align 8
  %16 = getelementptr inbounds %struct.list_head_s, ptr %15, i32 0, i32 1
  store ptr %14, ptr %16, align 8
  %17 = load ptr, ptr %4, align 8
  %18 = getelementptr inbounds %struct.list_head_s, ptr %17, i32 0, i32 0
  %19 = load ptr, ptr %18, align 8
  %20 = load ptr, ptr %3, align 8
  %21 = getelementptr inbounds %struct.list_head_s, ptr %20, i32 0, i32 0
  store ptr %19, ptr %21, align 8
  %22 = load ptr, ptr %3, align 8
  %23 = load ptr, ptr %4, align 8
  %24 = getelementptr inbounds %struct.list_head_s, ptr %23, i32 0, i32 0
  store ptr %22, ptr %24, align 8
  %25 = load ptr, ptr %3, align 8
  ret ptr %25
}

; Function Attrs: noinline nounwind  uwtable
define dso_local ptr @core_list_init(i64 noundef %0, ptr noundef %1, i16 noundef %2) #0 {
  %4 = alloca i64, align 8
  %5 = alloca ptr, align 8
  %6 = alloca i16, align 2
  %7 = alloca i64, align 8
  %8 = alloca i64, align 8
  %9 = alloca ptr, align 8
  %10 = alloca ptr, align 8
  %11 = alloca ptr, align 8
  %12 = alloca i64, align 8
  %13 = alloca ptr, align 8
  %14 = alloca ptr, align 8
  %15 = alloca %struct.list_data_s, align 2
  %16 = alloca i16, align 2
  %17 = alloca i16, align 2
  %18 = alloca i16, align 2
  store i64 %0, ptr %4, align 8
  store ptr %1, ptr %5, align 8
  store i16 %2, ptr %6, align 2
  store i64 20, ptr %7, align 8
  %19 = load i64, ptr %4, align 8
  store i64 %19, ptr %8, align 8
  %20 = load ptr, ptr %5, align 8
  %21 = load i64, ptr %8, align 8
  %22 = getelementptr inbounds %struct.list_head_s, ptr %20, i64 %21
  store ptr %22, ptr %9, align 8
  %23 = load ptr, ptr %9, align 8
  store ptr %23, ptr %10, align 8
  %24 = load ptr, ptr %10, align 8
  %25 = load i64, ptr %8, align 8
  %26 = getelementptr inbounds %struct.list_data_s, ptr %24, i64 %25
  store ptr %26, ptr %11, align 8
  %27 = load ptr, ptr %5, align 8
  store ptr %27, ptr %14, align 8
  %28 = load ptr, ptr %14, align 8
  %29 = getelementptr inbounds %struct.list_head_s, ptr %28, i32 0, i32 0
  store ptr null, ptr %29, align 8
  %30 = load ptr, ptr %10, align 8
  %31 = load ptr, ptr %14, align 8
  %32 = getelementptr inbounds %struct.list_head_s, ptr %31, i32 0, i32 1
  store ptr %30, ptr %32, align 8
  %33 = load ptr, ptr %14, align 8
  %34 = getelementptr inbounds %struct.list_head_s, ptr %33, i32 0, i32 1
  %35 = load ptr, ptr %34, align 8
  %36 = getelementptr inbounds %struct.list_data_s, ptr %35, i32 0, i32 1
  store i16 0, ptr %36, align 2
  %37 = load ptr, ptr %14, align 8
  %38 = getelementptr inbounds %struct.list_head_s, ptr %37, i32 0, i32 1
  %39 = load ptr, ptr %38, align 8
  %40 = getelementptr inbounds %struct.list_data_s, ptr %39, i32 0, i32 0
  store i16 -32640, ptr %40, align 2
  %41 = load ptr, ptr %5, align 8
  %42 = getelementptr inbounds %struct.list_head_s, ptr %41, i32 1
  store ptr %42, ptr %5, align 8
  %43 = load ptr, ptr %10, align 8
  %44 = getelementptr inbounds %struct.list_data_s, ptr %43, i32 1
  store ptr %44, ptr %10, align 8
  %45 = getelementptr inbounds %struct.list_data_s, ptr %15, i32 0, i32 1
  store i16 32767, ptr %45, align 2
  %46 = getelementptr inbounds %struct.list_data_s, ptr %15, i32 0, i32 0
  store i16 -1, ptr %46, align 2
  %47 = load ptr, ptr %14, align 8
  %48 = load ptr, ptr %9, align 8
  %49 = load ptr, ptr %11, align 8
  %50 = call ptr @core_list_insert_new(ptr noundef %47, ptr noundef %15, ptr noundef %5, ptr noundef %10, ptr noundef %48, ptr noundef %49)
  store i64 0, ptr %12, align 8
  br label %51

51:                                               ; preds = %84, %3
  %52 = load i64, ptr %12, align 8
  %53 = load i64, ptr %8, align 8
  %54 = icmp ult i64 %52, %53
  br i1 %54, label %55, label %87

55:                                               ; preds = %51
  %56 = load i16, ptr %6, align 2
  %57 = sext i16 %56 to i64
  %58 = load i64, ptr %12, align 8
  %59 = xor i64 %57, %58
  %60 = trunc i64 %59 to i16
  %61 = zext i16 %60 to i32
  %62 = and i32 %61, 15
  %63 = trunc i32 %62 to i16
  store i16 %63, ptr %16, align 2
  %64 = load i16, ptr %16, align 2
  %65 = zext i16 %64 to i32
  %66 = shl i32 %65, 3
  %67 = sext i32 %66 to i64
  %68 = load i64, ptr %12, align 8
  %69 = and i64 %68, 7
  %70 = or i64 %67, %69
  %71 = trunc i64 %70 to i16
  store i16 %71, ptr %17, align 2
  %72 = load i16, ptr %17, align 2
  %73 = zext i16 %72 to i32
  %74 = shl i32 %73, 8
  %75 = load i16, ptr %17, align 2
  %76 = zext i16 %75 to i32
  %77 = or i32 %74, %76
  %78 = trunc i32 %77 to i16
  %79 = getelementptr inbounds %struct.list_data_s, ptr %15, i32 0, i32 0
  store i16 %78, ptr %79, align 2
  %80 = load ptr, ptr %14, align 8
  %81 = load ptr, ptr %9, align 8
  %82 = load ptr, ptr %11, align 8
  %83 = call ptr @core_list_insert_new(ptr noundef %80, ptr noundef %15, ptr noundef %5, ptr noundef %10, ptr noundef %81, ptr noundef %82)
  br label %84

84:                                               ; preds = %55
  %85 = load i64, ptr %12, align 8
  %86 = add i64 %85, 1
  store i64 %86, ptr %12, align 8
  br label %51, !llvm.loop !16

87:                                               ; preds = %51
  %88 = load ptr, ptr %14, align 8
  %89 = getelementptr inbounds %struct.list_head_s, ptr %88, i32 0, i32 0
  %90 = load ptr, ptr %89, align 8
  store ptr %90, ptr %13, align 8
  store i64 1, ptr %12, align 8
  br label %91

91:                                               ; preds = %128, %87
  %92 = load ptr, ptr %13, align 8
  %93 = getelementptr inbounds %struct.list_head_s, ptr %92, i32 0, i32 0
  %94 = load ptr, ptr %93, align 8
  %95 = icmp ne ptr %94, null
  br i1 %95, label %96, label %132

96:                                               ; preds = %91
  %97 = load i64, ptr %12, align 8
  %98 = load i64, ptr %8, align 8
  %99 = udiv i64 %98, 5
  %100 = icmp ult i64 %97, %99
  br i1 %100, label %101, label %109

101:                                              ; preds = %96
  %102 = load i64, ptr %12, align 8
  %103 = add i64 %102, 1
  store i64 %103, ptr %12, align 8
  %104 = trunc i64 %102 to i16
  %105 = load ptr, ptr %13, align 8
  %106 = getelementptr inbounds %struct.list_head_s, ptr %105, i32 0, i32 1
  %107 = load ptr, ptr %106, align 8
  %108 = getelementptr inbounds %struct.list_data_s, ptr %107, i32 0, i32 1
  store i16 %104, ptr %108, align 2
  br label %128

109:                                              ; preds = %96
  %110 = load i64, ptr %12, align 8
  %111 = add i64 %110, 1
  store i64 %111, ptr %12, align 8
  %112 = load i16, ptr %6, align 2
  %113 = sext i16 %112 to i64
  %114 = xor i64 %110, %113
  %115 = trunc i64 %114 to i16
  store i16 %115, ptr %18, align 2
  %116 = load i64, ptr %12, align 8
  %117 = and i64 %116, 7
  %118 = shl i64 %117, 8
  %119 = load i16, ptr %18, align 2
  %120 = zext i16 %119 to i64
  %121 = or i64 %118, %120
  %122 = and i64 16383, %121
  %123 = trunc i64 %122 to i16
  %124 = load ptr, ptr %13, align 8
  %125 = getelementptr inbounds %struct.list_head_s, ptr %124, i32 0, i32 1
  %126 = load ptr, ptr %125, align 8
  %127 = getelementptr inbounds %struct.list_data_s, ptr %126, i32 0, i32 1
  store i16 %123, ptr %127, align 2
  br label %128

128:                                              ; preds = %109, %101
  %129 = load ptr, ptr %13, align 8
  %130 = getelementptr inbounds %struct.list_head_s, ptr %129, i32 0, i32 0
  %131 = load ptr, ptr %130, align 8
  store ptr %131, ptr %13, align 8
  br label %91, !llvm.loop !17

132:                                              ; preds = %91
  %133 = load ptr, ptr %14, align 8
  %134 = call ptr @core_list_mergesort(ptr noundef %133, ptr noundef @cmp_idx, ptr noundef null)
  store ptr %134, ptr %14, align 8
  %135 = load ptr, ptr %14, align 8
  ret ptr %135
}

; Function Attrs: noinline nounwind  uwtable
define dso_local ptr @core_list_insert_new(ptr noundef %0, ptr noundef %1, ptr noundef %2, ptr noundef %3, ptr noundef %4, ptr noundef %5) #0 {
  %7 = alloca ptr, align 8
  %8 = alloca ptr, align 8
  %9 = alloca ptr, align 8
  %10 = alloca ptr, align 8
  %11 = alloca ptr, align 8
  %12 = alloca ptr, align 8
  %13 = alloca ptr, align 8
  %14 = alloca ptr, align 8
  store ptr %0, ptr %8, align 8
  store ptr %1, ptr %9, align 8
  store ptr %2, ptr %10, align 8
  store ptr %3, ptr %11, align 8
  store ptr %4, ptr %12, align 8
  store ptr %5, ptr %13, align 8
  %15 = load ptr, ptr %10, align 8
  %16 = load ptr, ptr %15, align 8
  %17 = getelementptr inbounds %struct.list_head_s, ptr %16, i64 1
  %18 = load ptr, ptr %12, align 8
  %19 = icmp uge ptr %17, %18
  br i1 %19, label %20, label %21

20:                                               ; preds = %6
  store ptr null, ptr %7, align 8
  br label %54

21:                                               ; preds = %6
  %22 = load ptr, ptr %11, align 8
  %23 = load ptr, ptr %22, align 8
  %24 = getelementptr inbounds %struct.list_data_s, ptr %23, i64 1
  %25 = load ptr, ptr %13, align 8
  %26 = icmp uge ptr %24, %25
  br i1 %26, label %27, label %28

27:                                               ; preds = %21
  store ptr null, ptr %7, align 8
  br label %54

28:                                               ; preds = %21
  %29 = load ptr, ptr %10, align 8
  %30 = load ptr, ptr %29, align 8
  store ptr %30, ptr %14, align 8
  %31 = load ptr, ptr %10, align 8
  %32 = load ptr, ptr %31, align 8
  %33 = getelementptr inbounds %struct.list_head_s, ptr %32, i32 1
  store ptr %33, ptr %31, align 8
  %34 = load ptr, ptr %8, align 8
  %35 = getelementptr inbounds %struct.list_head_s, ptr %34, i32 0, i32 0
  %36 = load ptr, ptr %35, align 8
  %37 = load ptr, ptr %14, align 8
  %38 = getelementptr inbounds %struct.list_head_s, ptr %37, i32 0, i32 0
  store ptr %36, ptr %38, align 8
  %39 = load ptr, ptr %14, align 8
  %40 = load ptr, ptr %8, align 8
  %41 = getelementptr inbounds %struct.list_head_s, ptr %40, i32 0, i32 0
  store ptr %39, ptr %41, align 8
  %42 = load ptr, ptr %11, align 8
  %43 = load ptr, ptr %42, align 8
  %44 = load ptr, ptr %14, align 8
  %45 = getelementptr inbounds %struct.list_head_s, ptr %44, i32 0, i32 1
  store ptr %43, ptr %45, align 8
  %46 = load ptr, ptr %11, align 8
  %47 = load ptr, ptr %46, align 8
  %48 = getelementptr inbounds %struct.list_data_s, ptr %47, i32 1
  store ptr %48, ptr %46, align 8
  %49 = load ptr, ptr %14, align 8
  %50 = getelementptr inbounds %struct.list_head_s, ptr %49, i32 0, i32 1
  %51 = load ptr, ptr %50, align 8
  %52 = load ptr, ptr %9, align 8
  call void @copy_info(ptr noundef %51, ptr noundef %52)
  %53 = load ptr, ptr %14, align 8
  store ptr %53, ptr %7, align 8
  br label %54

54:                                               ; preds = %28, %27, %20
  %55 = load ptr, ptr %7, align 8
  ret ptr %55
}

attributes #0 = { noinline nounwind  uwtable "frame-pointer"="non-leaf" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "target-features"="+fp-armv8,+neon,+v8a,-fmv" }
attributes #1 = { "frame-pointer"="non-leaf" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "target-features"="+fp-armv8,+neon,+v8a,-fmv" }

!llvm.module.flags = !{!0, !1, !2, !3, !4}
!llvm.ident = !{!5}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{i32 8, !"PIC Level", i32 2}
!2 = !{i32 7, !"PIE Level", i32 2}
!3 = !{i32 7, !"uwtable", i32 2}
!4 = !{i32 7, !"frame-pointer", i32 1}
!5 = !{!"clang version 18.1.5 (/home/muke/Programming/Memory_Renaming/llvm/clang 32112e08a9133b16e141f748a7a5c8376eb7cf5c)"}
!6 = distinct !{!6, !7}
!7 = !{!"llvm.loop.mustprogress"}
!8 = distinct !{!8, !7}
!9 = distinct !{!9, !7}
!10 = distinct !{!10, !7}
!11 = distinct !{!11, !7}
!12 = distinct !{!12, !7}
!13 = distinct !{!13, !7}
!14 = distinct !{!14, !7}
!15 = distinct !{!15, !7}
!16 = distinct !{!16, !7}
!17 = distinct !{!17, !7}
