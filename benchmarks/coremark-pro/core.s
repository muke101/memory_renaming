	.text
	.file	"core_list_join.c"
	.globl	calc_func                       // -- Begin function calc_func
	.p2align	2
	.type	calc_func,@function
calc_func:                              // @calc_func
	.cfi_startproc
// %bb.0:
	stp	x29, x30, [sp, #-48]!           // 16-byte Folded Spill
	.cfi_def_cfa_offset 48
	stp	x22, x21, [sp, #16]             // 16-byte Folded Spill
	stp	x20, x19, [sp, #32]             // 16-byte Folded Spill
	mov	x29, sp
	.cfi_def_cfa w29, 48
	.cfi_offset w19, -8
	.cfi_offset w20, -16
	.cfi_offset w21, -24
	.cfi_offset w22, -32
	.cfi_offset w30, -40
	.cfi_offset w29, -48
	.cfi_remember_state
	ldrh	w22, [x0]
	tbnz	w22, #7, .LBB0_5
// %bb.1:
	ubfx	w9, w22, #3, #4
	mov	x20, x1
	and	w8, w22, #0x7
	mov	x19, x0
	cmp	w8, #1
	orr	w1, w9, w9, lsl #4
	b.eq	.LBB0_6
// %bb.2:
	mov	w21, w22
	cbnz	w8, .LBB0_8
// %bb.3:
	ldp	x8, x0, [x20, #32]
	cmp	w1, #34
	mov	w9, #34                         // =0x22
	ldrh	w5, [x20, #104]
	ldrh	w3, [x20, #2]
	ldrh	w2, [x20]
	csel	w4, w1, w9, hi
	mov	x1, x8
	bl	core_bench_state
	ldrh	w8, [x20, #110]
	mov	w21, w0
	cbnz	w8, .LBB0_8
// %bb.4:
	strh	w21, [x20, #110]
	b	.LBB0_8
.LBB0_5:
	and	w0, w22, #0x7f
	.cfi_def_cfa wsp, 48
	ldp	x20, x19, [sp, #32]             // 16-byte Folded Reload
	ldp	x22, x21, [sp, #16]             // 16-byte Folded Reload
	ldp	x29, x30, [sp], #48             // 16-byte Folded Reload
	.cfi_def_cfa_offset 0
	.cfi_restore w19
	.cfi_restore w20
	.cfi_restore w21
	.cfi_restore w22
	.cfi_restore w30
	.cfi_restore w29
	ret
.LBB0_6:
	.cfi_restore_state
	ldrh	w2, [x20, #104]
	add	x0, x20, #72
	bl	core_bench_matrix
	ldrh	w8, [x20, #108]
	mov	w21, w0
	cbnz	w8, .LBB0_8
// %bb.7:
	strh	w21, [x20, #108]
.LBB0_8:
	ldrh	w1, [x20, #104]
	mov	w0, w21
	bl	crcu16
	and	w8, w22, #0xff00
	strh	w0, [x20, #104]
	and	w0, w21, #0x7f
	bfxil	w8, w21, #0, #7
	orr	w8, w8, #0x80
	strh	w8, [x19]
	.cfi_def_cfa wsp, 48
	ldp	x20, x19, [sp, #32]             // 16-byte Folded Reload
	ldp	x22, x21, [sp, #16]             // 16-byte Folded Reload
	ldp	x29, x30, [sp], #48             // 16-byte Folded Reload
	.cfi_def_cfa_offset 0
	.cfi_restore w19
	.cfi_restore w20
	.cfi_restore w21
	.cfi_restore w22
	.cfi_restore w30
	.cfi_restore w29
	ret
.Lfunc_end0:
	.size	calc_func, .Lfunc_end0-calc_func
	.cfi_endproc
                                        // -- End function
	.globl	cmp_complex                     // -- Begin function cmp_complex
	.p2align	2
	.type	cmp_complex,@function
cmp_complex:                            // @cmp_complex
	.cfi_startproc
// %bb.0:
	stp	x29, x30, [sp, #-48]!           // 16-byte Folded Spill
	.cfi_def_cfa_offset 48
	str	x21, [sp, #16]                  // 8-byte Folded Spill
	stp	x20, x19, [sp, #32]             // 16-byte Folded Spill
	mov	x29, sp
	.cfi_def_cfa w29, 48
	.cfi_offset w19, -8
	.cfi_offset w20, -16
	.cfi_offset w21, -32
	.cfi_offset w30, -40
	.cfi_offset w29, -48
	mov	x20, x1
	mov	x1, x2
	mov	x19, x2
	bl	calc_func
	mov	w21, w0
	mov	x0, x20
	mov	x1, x19
	bl	calc_func
	and	x8, x21, #0xffff
                                        // kill: def $w0 killed $w0 def $x0
	sub	x0, x8, w0, uxth
	.cfi_def_cfa wsp, 48
	ldp	x20, x19, [sp, #32]             // 16-byte Folded Reload
	ldr	x21, [sp, #16]                  // 8-byte Folded Reload
	ldp	x29, x30, [sp], #48             // 16-byte Folded Reload
	.cfi_def_cfa_offset 0
	.cfi_restore w19
	.cfi_restore w20
	.cfi_restore w21
	.cfi_restore w30
	.cfi_restore w29
	ret
.Lfunc_end1:
	.size	cmp_complex, .Lfunc_end1-cmp_complex
	.cfi_endproc
                                        // -- End function
	.globl	cmp_idx                         // -- Begin function cmp_idx
	.p2align	2
	.type	cmp_idx,@function
cmp_idx:                                // @cmp_idx
	.cfi_startproc
// %bb.0:
	cbz	x2, .LBB2_2
// %bb.1:
	ldrsh	x8, [x0, #2]
	ldrsh	x9, [x1, #2]
	sub	x0, x8, x9
	ret
.LBB2_2:
	ldrb	w8, [x0, #1]
	strb	w8, [x0]
	ldrb	w8, [x1, #1]
	strb	w8, [x1]
	ldrsh	x8, [x0, #2]
	ldrsh	x9, [x1, #2]
	sub	x0, x8, x9
	ret
.Lfunc_end2:
	.size	cmp_idx, .Lfunc_end2-cmp_idx
	.cfi_endproc
                                        // -- End function
	.globl	copy_info                       // -- Begin function copy_info
	.p2align	2
	.type	copy_info,@function
copy_info:                              // @copy_info
	.cfi_startproc
// %bb.0:
	ldr	w8, [x1]
	str	w8, [x0]
	ret
.Lfunc_end3:
	.size	copy_info, .Lfunc_end3-copy_info
	.cfi_endproc
                                        // -- End function
	.globl	core_bench_list                 // -- Begin function core_bench_list
	.p2align	2
	.type	core_bench_list,@function
core_bench_list:                        // @core_bench_list
	.cfi_startproc
// %bb.0:
	sub	sp, sp, #144
	.cfi_def_cfa_offset 144
	stp	x29, x30, [sp, #48]             // 16-byte Folded Spill
	stp	x28, x27, [sp, #64]             // 16-byte Folded Spill
	stp	x26, x25, [sp, #80]             // 16-byte Folded Spill
	stp	x24, x23, [sp, #96]             // 16-byte Folded Spill
	stp	x22, x21, [sp, #112]            // 16-byte Folded Spill
	stp	x20, x19, [sp, #128]            // 16-byte Folded Spill
	add	x29, sp, #48
	.cfi_def_cfa w29, 96
	.cfi_offset w19, -8
	.cfi_offset w20, -16
	.cfi_offset w21, -24
	.cfi_offset w22, -32
	.cfi_offset w23, -40
	.cfi_offset w24, -48
	.cfi_offset w25, -56
	.cfi_offset w26, -64
	.cfi_offset w27, -72
	.cfi_offset w28, -80
	.cfi_offset w30, -88
	.cfi_offset w29, -96
	ldrsh	w9, [x0, #4]
	ldr	x22, [x0, #64]
	mov	x20, x0
	subs	w8, w9, #1
	b.lt	.LBB4_21
// %bb.1:
	mov	w13, wzr
	mov	w10, wzr
	mov	w11, wzr
	mov	w12, wzr
	mov	w14, #32768                     // =0x8000
	mov	w2, w1
	b	.LBB4_5
.LBB4_2:                                //   in Loop: Header=BB4_5 Depth=1
	mov	x16, xzr
.LBB4_3:                                //   in Loop: Header=BB4_5 Depth=1
	ldr	x15, [x16]
	add	w12, w12, #1
	ldr	x15, [x15, #8]
	ldrh	w15, [x15]
	ubfx	w17, w15, #8, #1
.LBB4_4:                                //   in Loop: Header=BB4_5 Depth=1
	bic	w15, w14, w2
	add	w13, w13, #1
	mov	x22, x16
	add	w2, w2, w15, lsr #15
	cmp	w9, w13, sxth
	add	w10, w17, w10
	b.le	.LBB4_20
.LBB4_5:                                // =>This Loop Header: Depth=1
                                        //     Child Loop BB4_11 Depth 2
                                        //     Child Loop BB4_8 Depth 2
                                        //     Child Loop BB4_15 Depth 2
	tbnz	w2, #15, .LBB4_10
// %bb.6:                               //   in Loop: Header=BB4_5 Depth=1
	cbz	x22, .LBB4_2
// %bb.7:                               //   in Loop: Header=BB4_5 Depth=1
	mov	x15, x22
.LBB4_8:                                //   Parent Loop BB4_5 Depth=1
                                        // =>  This Inner Loop Header: Depth=2
	ldr	x16, [x15, #8]
	ldrh	w16, [x16, #2]
	cmp	w16, w2, uxth
	b.eq	.LBB4_13
// %bb.9:                               //   in Loop: Header=BB4_8 Depth=2
	ldr	x15, [x15]
	cbnz	x15, .LBB4_8
	b	.LBB4_13
.LBB4_10:                               //   in Loop: Header=BB4_5 Depth=1
	mov	x15, x22
	cbz	x22, .LBB4_2
.LBB4_11:                               //   Parent Loop BB4_5 Depth=1
                                        // =>  This Inner Loop Header: Depth=2
	ldr	x16, [x15, #8]
	and	w17, w13, #0xff
	ldrb	w16, [x16]
	eor	w16, w16, w17
	cbz	w16, .LBB4_13
// %bb.12:                              //   in Loop: Header=BB4_11 Depth=2
	ldr	x15, [x15]
	cbnz	x15, .LBB4_11
.LBB4_13:                               //   in Loop: Header=BB4_5 Depth=1
	cbz	x22, .LBB4_17
// %bb.14:                              //   in Loop: Header=BB4_5 Depth=1
	mov	x17, xzr
.LBB4_15:                               //   Parent Loop BB4_5 Depth=1
                                        // =>  This Inner Loop Header: Depth=2
	mov	x16, x22
	ldr	x22, [x22]
	str	x17, [x16]
	mov	x17, x16
	cbnz	x22, .LBB4_15
// %bb.16:                              //   in Loop: Header=BB4_5 Depth=1
	cbnz	x15, .LBB4_18
	b	.LBB4_3
.LBB4_17:                               //   in Loop: Header=BB4_5 Depth=1
	mov	x16, xzr
	cbz	x15, .LBB4_3
.LBB4_18:                               //   in Loop: Header=BB4_5 Depth=1
	ldp	x18, x17, [x15]
	add	w11, w11, #1
	ldrh	w17, [x17]
	and	w17, w17, w17, lsr #9
	and	w17, w17, #0x1
	cbz	x18, .LBB4_4
// %bb.19:                              //   in Loop: Header=BB4_5 Depth=1
	ldr	x0, [x18]
	str	x0, [x15]
	ldr	x15, [x16]
	str	x15, [x18]
	str	x18, [x16]
	b	.LBB4_4
.LBB4_20:
	sub	w9, w10, w12
	and	w10, w8, #0xff
	add	w9, w9, w11, lsl #2
	sxth	w8, w1
	stp	w2, w9, [sp, #8]                // 8-byte Folded Spill
	str	w10, [sp, #4]                   // 4-byte Folded Spill
	cmp	w8, #1
	b.ge	.LBB4_22
	b	.LBB4_58
.LBB4_21:
	mov	w9, wzr
	mov	w2, w1
                                        // implicit-def: $w10
	sxth	w8, w1
	stp	w1, wzr, [sp, #8]               // 8-byte Folded Spill
	str	w10, [sp, #4]                   // 4-byte Folded Spill
	cmp	w8, #1
	b.lt	.LBB4_58
.LBB4_22:
	mov	w19, #1                         // =0x1
	b	.LBB4_24
.LBB4_23:                               //   in Loop: Header=BB4_24 Depth=1
	lsl	x19, x19, #1
	ldur	x22, [x29, #-8]                 // 8-byte Folded Reload
	ldr	x8, [sp, #16]                   // 8-byte Folded Reload
	str	xzr, [x24]
	cbz	x8, .LBB4_58
.LBB4_24:                               // =>This Loop Header: Depth=1
                                        //     Child Loop BB4_26 Depth 2
                                        //       Child Loop BB4_27 Depth 3
	mov	x8, xzr
	mov	x24, xzr
	stur	xzr, [x29, #-8]                 // 8-byte Folded Spill
	str	x19, [sp, #24]                  // 8-byte Folded Spill
	b	.LBB4_26
.LBB4_25:                               //   in Loop: Header=BB4_26 Depth=2
	ldr	x19, [sp, #24]                  // 8-byte Folded Reload
	ldur	x8, [x29, #-16]                 // 8-byte Folded Reload
	cbz	x22, .LBB4_23
.LBB4_26:                               //   Parent Loop BB4_24 Depth=1
                                        // =>  This Loop Header: Depth=2
                                        //       Child Loop BB4_27 Depth 3
	mov	x25, xzr
	str	x8, [sp, #16]                   // 8-byte Folded Spill
	add	x9, x8, #1
	mov	x8, x22
.LBB4_27:                               //   Parent Loop BB4_24 Depth=1
                                        //     Parent Loop BB4_26 Depth=2
                                        // =>    This Inner Loop Header: Depth=3
	ldr	x8, [x8]
	add	x25, x25, #1
	cbz	x8, .LBB4_30
// %bb.28:                              //   in Loop: Header=BB4_27 Depth=3
	cmp	x19, x25
	b.ne	.LBB4_27
// %bb.29:                              //   in Loop: Header=BB4_26 Depth=2
	mov	x25, x19
.LBB4_30:                               //   in Loop: Header=BB4_26 Depth=2
	stur	x9, [x29, #-16]                 // 8-byte Folded Spill
	mov	x9, x22
	mov	x28, x9
	mov	x22, x8
	cmp	x25, #0
	b.gt	.LBB4_35
.LBB4_31:                               //   in Loop: Header=BB4_26 Depth=2
	subs	x11, x19, #1
	b.lt	.LBB4_25
// %bb.32:                              //   in Loop: Header=BB4_26 Depth=2
	cbz	x22, .LBB4_25
// %bb.33:                              //   in Loop: Header=BB4_26 Depth=2
	cbnz	x25, .LBB4_35
// %bb.34:                              //   in Loop: Header=BB4_26 Depth=2
	ldr	x8, [x22]
	mov	x9, x28
	mov	x10, x22
	mov	x19, x11
	cbnz	x24, .LBB4_56
	b	.LBB4_57
.LBB4_35:                               //   in Loop: Header=BB4_26 Depth=2
	cbz	x19, .LBB4_44
// %bb.36:                              //   in Loop: Header=BB4_26 Depth=2
	cbz	x22, .LBB4_44
// %bb.37:                              //   in Loop: Header=BB4_26 Depth=2
	ldr	x27, [x28, #8]
	ldr	x23, [x22, #8]
	ldrh	w26, [x27]
	tbnz	w26, #7, .LBB4_42
// %bb.38:                              //   in Loop: Header=BB4_26 Depth=2
	ubfx	w9, w26, #3, #4
	and	w8, w26, #0x7
	cmp	w8, #1
	orr	w1, w9, w9, lsl #4
	b.eq	.LBB4_45
// %bb.39:                              //   in Loop: Header=BB4_26 Depth=2
	mov	w21, w26
	cbnz	w8, .LBB4_47
// %bb.40:                              //   in Loop: Header=BB4_26 Depth=2
	ldp	x8, x0, [x20, #32]
	cmp	w1, #34
	ldrh	w5, [x20, #104]
	ldrh	w3, [x20, #2]
	mov	w9, #34                         // =0x22
	ldrh	w2, [x20]
	csel	w4, w1, w9, hi
	mov	x1, x8
	bl	core_bench_state
	ldrh	w8, [x20, #110]
	mov	w21, w0
	cbnz	w8, .LBB4_47
// %bb.41:                              //   in Loop: Header=BB4_26 Depth=2
	strh	w21, [x20, #110]
	b	.LBB4_47
.LBB4_42:                               //   in Loop: Header=BB4_26 Depth=2
	and	w26, w26, #0x7f
	ldrh	w27, [x23]
	tbz	w27, #7, .LBB4_48
.LBB4_43:                               //   in Loop: Header=BB4_26 Depth=2
	and	w8, w27, #0x7f
	cmp	w26, w8
	b.hi	.LBB4_55
.LBB4_44:                               //   in Loop: Header=BB4_26 Depth=2
	ldr	x9, [x28]
	sub	x25, x25, #1
	mov	x8, x22
	mov	x10, x28
	cbnz	x24, .LBB4_56
	b	.LBB4_57
.LBB4_45:                               //   in Loop: Header=BB4_26 Depth=2
	ldrh	w2, [x20, #104]
	add	x0, x20, #72
	bl	core_bench_matrix
	ldrh	w8, [x20, #108]
	mov	w21, w0
	cbnz	w8, .LBB4_47
// %bb.46:                              //   in Loop: Header=BB4_26 Depth=2
	strh	w21, [x20, #108]
.LBB4_47:                               //   in Loop: Header=BB4_26 Depth=2
	ldrh	w1, [x20, #104]
	mov	w0, w21
	bl	crcu16
	and	w8, w26, #0xff00
	and	w26, w21, #0x7f
	strh	w0, [x20, #104]
	bfxil	w8, w21, #0, #7
	orr	w8, w8, #0x80
	strh	w8, [x27]
	ldrh	w27, [x23]
	tbnz	w27, #7, .LBB4_43
.LBB4_48:                               //   in Loop: Header=BB4_26 Depth=2
	ubfx	w9, w27, #3, #4
	and	w8, w27, #0x7
	cmp	w8, #1
	orr	w1, w9, w9, lsl #4
	b.eq	.LBB4_52
// %bb.49:                              //   in Loop: Header=BB4_26 Depth=2
	mov	w21, w27
	cbnz	w8, .LBB4_54
// %bb.50:                              //   in Loop: Header=BB4_26 Depth=2
	ldp	x8, x0, [x20, #32]
	cmp	w1, #34
	ldrh	w5, [x20, #104]
	ldrh	w3, [x20, #2]
	mov	w9, #34                         // =0x22
	ldrh	w2, [x20]
	csel	w4, w1, w9, hi
	mov	x1, x8
	bl	core_bench_state
	ldrh	w8, [x20, #110]
	mov	w21, w0
	cbnz	w8, .LBB4_54
// %bb.51:                              //   in Loop: Header=BB4_26 Depth=2
	strh	w21, [x20, #110]
	b	.LBB4_54
.LBB4_52:                               //   in Loop: Header=BB4_26 Depth=2
	ldrh	w2, [x20, #104]
	add	x0, x20, #72
	bl	core_bench_matrix
	ldrh	w8, [x20, #108]
	mov	w21, w0
	cbnz	w8, .LBB4_54
// %bb.53:                              //   in Loop: Header=BB4_26 Depth=2
	strh	w21, [x20, #108]
.LBB4_54:                               //   in Loop: Header=BB4_26 Depth=2
	ldrh	w1, [x20, #104]
	mov	w0, w21
	bl	crcu16
	and	w8, w27, #0xff00
	strh	w0, [x20, #104]
	bfxil	w8, w21, #0, #7
	orr	w9, w8, #0x80
	and	w8, w21, #0x7f
	strh	w9, [x23]
	cmp	w26, w8
	b.ls	.LBB4_44
.LBB4_55:                               //   in Loop: Header=BB4_26 Depth=2
	ldr	x8, [x22]
	sub	x19, x19, #1
	mov	x9, x28
	mov	x10, x22
	cbz	x24, .LBB4_57
.LBB4_56:                               //   in Loop: Header=BB4_26 Depth=2
	str	x10, [x24]
	mov	x28, x9
	mov	x22, x8
	mov	x24, x10
	cmp	x25, #0
	b.gt	.LBB4_35
	b	.LBB4_31
.LBB4_57:                               //   in Loop: Header=BB4_26 Depth=2
	stur	x10, [x29, #-8]                 // 8-byte Folded Spill
	mov	x24, x10
	mov	x28, x9
	mov	x22, x8
	cmp	x25, #0
	b.gt	.LBB4_35
	b	.LBB4_31
.LBB4_58:
	ldr	x9, [x22]
	ldr	w10, [sp, #8]                   // 4-byte Folded Reload
	ldp	x19, x8, [x9]
	ldr	q0, [x19]
	str	q0, [x9]
	stp	xzr, x8, [x19]
	tbnz	w10, #15, .LBB4_62
// %bb.59:
	ldr	w1, [sp, #12]                   // 4-byte Folded Reload
	mov	x20, x22
.LBB4_60:                               // =>This Inner Loop Header: Depth=1
	ldr	x9, [x20, #8]
	ldrh	w9, [x9, #2]
	cmp	w9, w10, uxth
	b.eq	.LBB4_66
// %bb.61:                              //   in Loop: Header=BB4_60 Depth=1
	ldr	x20, [x20]
	cbnz	x20, .LBB4_60
	b	.LBB4_65
.LBB4_62:
	ldr	w1, [sp, #12]                   // 4-byte Folded Reload
	ldr	w10, [sp, #4]                   // 4-byte Folded Reload
	mov	x20, x22
.LBB4_63:                               // =>This Inner Loop Header: Depth=1
	ldr	x9, [x20, #8]
	ldrb	w9, [x9]
	cmp	w9, w10, uxth
	b.eq	.LBB4_66
// %bb.64:                              //   in Loop: Header=BB4_63 Depth=1
	ldr	x20, [x20]
	cbnz	x20, .LBB4_63
.LBB4_65:
	ldr	x20, [x22]
	cbz	x20, .LBB4_68
.LBB4_66:                               // =>This Inner Loop Header: Depth=1
	ldr	x8, [x22, #8]
	ldrh	w0, [x8]
	bl	crc16
	ldr	x20, [x20]
	mov	w1, w0
	cbnz	x20, .LBB4_66
// %bb.67:
	ldr	x8, [x19, #8]
.LBB4_68:
	ldr	x9, [x22]
	ldr	q0, [x9]
	str	q0, [x19]
	stp	x19, x8, [x9]
	mov	w8, #1                          // =0x1
	b	.LBB4_70
.LBB4_69:                               //   in Loop: Header=BB4_70 Depth=1
	lsl	x8, x8, #1
	str	xzr, [x11]
	cbz	x9, .LBB4_88
.LBB4_70:                               // =>This Loop Header: Depth=1
                                        //     Child Loop BB4_72 Depth 2
                                        //       Child Loop BB4_73 Depth 3
	mov	x10, xzr
	mov	x11, xzr
	mov	x13, x22
	mov	x22, xzr
	b	.LBB4_72
.LBB4_71:                               //   in Loop: Header=BB4_72 Depth=2
	cbz	x13, .LBB4_69
.LBB4_72:                               //   Parent Loop BB4_70 Depth=1
                                        // =>  This Loop Header: Depth=2
                                        //       Child Loop BB4_73 Depth 3
	mov	x12, xzr
	mov	x9, x10
	add	x10, x10, #1
	mov	x15, x13
.LBB4_73:                               //   Parent Loop BB4_70 Depth=1
                                        //     Parent Loop BB4_72 Depth=2
                                        // =>    This Inner Loop Header: Depth=3
	ldr	x15, [x15]
	add	x12, x12, #1
	cbz	x15, .LBB4_76
// %bb.74:                              //   in Loop: Header=BB4_73 Depth=3
	cmp	x8, x12
	b.ne	.LBB4_73
// %bb.75:                              //   in Loop: Header=BB4_72 Depth=2
	mov	x12, x8
.LBB4_76:                               //   in Loop: Header=BB4_72 Depth=2
	mov	x17, x13
	mov	x14, x8
	mov	x16, x17
	mov	x13, x15
	cmp	x12, #0
	b.gt	.LBB4_81
.LBB4_77:                               //   in Loop: Header=BB4_72 Depth=2
	subs	x0, x14, #1
	b.lt	.LBB4_71
// %bb.78:                              //   in Loop: Header=BB4_72 Depth=2
	cbz	x13, .LBB4_71
// %bb.79:                              //   in Loop: Header=BB4_72 Depth=2
	cbnz	x12, .LBB4_81
// %bb.80:                              //   in Loop: Header=BB4_72 Depth=2
	ldr	x15, [x13]
	mov	x17, x16
	mov	x18, x13
	mov	x14, x0
	cbnz	x11, .LBB4_86
	b	.LBB4_87
.LBB4_81:                               //   in Loop: Header=BB4_72 Depth=2
	cbz	x14, .LBB4_85
// %bb.82:                              //   in Loop: Header=BB4_72 Depth=2
	cbz	x13, .LBB4_85
// %bb.83:                              //   in Loop: Header=BB4_72 Depth=2
	ldr	x15, [x16, #8]
	ldr	x18, [x13, #8]
	ldrb	w17, [x15, #1]
	strb	w17, [x15]
	ldrb	w17, [x18, #1]
	strb	w17, [x18]
	ldrsh	w17, [x18, #2]
	ldrsh	w15, [x15, #2]
	cmp	w15, w17
	b.le	.LBB4_85
// %bb.84:                              //   in Loop: Header=BB4_72 Depth=2
	ldr	x15, [x13]
	sub	x14, x14, #1
	mov	x17, x16
	mov	x18, x13
	cbnz	x11, .LBB4_86
	b	.LBB4_87
.LBB4_85:                               //   in Loop: Header=BB4_72 Depth=2
	ldr	x17, [x16]
	sub	x12, x12, #1
	mov	x15, x13
	mov	x18, x16
	cbz	x11, .LBB4_87
.LBB4_86:                               //   in Loop: Header=BB4_72 Depth=2
	str	x18, [x11]
	mov	x16, x17
	mov	x13, x15
	mov	x11, x18
	cmp	x12, #0
	b.gt	.LBB4_81
	b	.LBB4_77
.LBB4_87:                               //   in Loop: Header=BB4_72 Depth=2
	mov	x22, x18
	mov	x11, x18
	mov	x16, x17
	mov	x13, x15
	cmp	x12, #0
	b.gt	.LBB4_81
	b	.LBB4_77
.LBB4_88:
	ldr	x19, [x22]
	cbz	x19, .LBB4_90
.LBB4_89:                               // =>This Inner Loop Header: Depth=1
	ldr	x8, [x22, #8]
	ldrh	w0, [x8]
	bl	crc16
	ldr	x19, [x19]
	mov	w1, w0
	cbnz	x19, .LBB4_89
.LBB4_90:
	mov	w0, w1
	.cfi_def_cfa wsp, 144
	ldp	x20, x19, [sp, #128]            // 16-byte Folded Reload
	ldp	x22, x21, [sp, #112]            // 16-byte Folded Reload
	ldp	x24, x23, [sp, #96]             // 16-byte Folded Reload
	ldp	x26, x25, [sp, #80]             // 16-byte Folded Reload
	ldp	x28, x27, [sp, #64]             // 16-byte Folded Reload
	ldp	x29, x30, [sp, #48]             // 16-byte Folded Reload
	add	sp, sp, #144
	.cfi_def_cfa_offset 0
	.cfi_restore w19
	.cfi_restore w20
	.cfi_restore w21
	.cfi_restore w22
	.cfi_restore w23
	.cfi_restore w24
	.cfi_restore w25
	.cfi_restore w26
	.cfi_restore w27
	.cfi_restore w28
	.cfi_restore w30
	.cfi_restore w29
	ret
.Lfunc_end4:
	.size	core_bench_list, .Lfunc_end4-core_bench_list
	.cfi_endproc
                                        // -- End function
	.globl	core_list_find                  // -- Begin function core_list_find
	.p2align	2
	.type	core_list_find,@function
core_list_find:                         // @core_list_find
	.cfi_startproc
// %bb.0:
	ldrsh	w8, [x1, #2]
	tbnz	w8, #31, .LBB5_5
// %bb.1:
	cbz	x0, .LBB5_9
// %bb.2:
	and	w8, w8, #0xffff
.LBB5_3:                                // =>This Inner Loop Header: Depth=1
	ldr	x9, [x0, #8]
	ldrh	w9, [x9, #2]
	cmp	w9, w8
	b.eq	.LBB5_9
// %bb.4:                               //   in Loop: Header=BB5_3 Depth=1
	ldr	x0, [x0]
	cbnz	x0, .LBB5_3
	b	.LBB5_9
.LBB5_5:
	cbz	x0, .LBB5_9
// %bb.6:
	ldrh	w8, [x1]
.LBB5_7:                                // =>This Inner Loop Header: Depth=1
	ldr	x9, [x0, #8]
	ldrb	w9, [x9]
	cmp	w9, w8
	b.eq	.LBB5_9
// %bb.8:                               //   in Loop: Header=BB5_7 Depth=1
	ldr	x0, [x0]
	cbnz	x0, .LBB5_7
.LBB5_9:
	ret
.Lfunc_end5:
	.size	core_list_find, .Lfunc_end5-core_list_find
	.cfi_endproc
                                        // -- End function
	.globl	core_list_reverse               // -- Begin function core_list_reverse
	.p2align	2
	.type	core_list_reverse,@function
core_list_reverse:                      // @core_list_reverse
	.cfi_startproc
// %bb.0:
	cbz	x0, .LBB6_4
// %bb.1:
	mov	x9, xzr
.LBB6_2:                                // =>This Inner Loop Header: Depth=1
	mov	x8, x0
	ldr	x0, [x0]
	str	x9, [x8]
	mov	x9, x8
	cbnz	x0, .LBB6_2
// %bb.3:
	mov	x0, x8
	ret
.LBB6_4:
	mov	x8, xzr
	mov	x0, x8
	ret
.Lfunc_end6:
	.size	core_list_reverse, .Lfunc_end6-core_list_reverse
	.cfi_endproc
                                        // -- End function
	.globl	core_list_mergesort             // -- Begin function core_list_mergesort
	.p2align	2
	.type	core_list_mergesort,@function
core_list_mergesort:                    // @core_list_mergesort
	.cfi_startproc
// %bb.0:
	sub	sp, sp, #112
	.cfi_def_cfa_offset 112
	stp	x29, x30, [sp, #16]             // 16-byte Folded Spill
	stp	x28, x27, [sp, #32]             // 16-byte Folded Spill
	stp	x26, x25, [sp, #48]             // 16-byte Folded Spill
	stp	x24, x23, [sp, #64]             // 16-byte Folded Spill
	stp	x22, x21, [sp, #80]             // 16-byte Folded Spill
	stp	x20, x19, [sp, #96]             // 16-byte Folded Spill
	add	x29, sp, #16
	.cfi_def_cfa w29, 96
	.cfi_offset w19, -8
	.cfi_offset w20, -16
	.cfi_offset w21, -24
	.cfi_offset w22, -32
	.cfi_offset w23, -40
	.cfi_offset w24, -48
	.cfi_offset w25, -56
	.cfi_offset w26, -64
	.cfi_offset w27, -72
	.cfi_offset w28, -80
	.cfi_offset w30, -88
	.cfi_offset w29, -96
	mov	x19, x2
	mov	x20, x1
	mov	x21, x0
	mov	w22, #1                         // =0x1
	b	.LBB7_2
.LBB7_1:                                //   in Loop: Header=BB7_2 Depth=1
	lsl	x22, x22, #1
	ldr	x8, [sp, #8]                    // 8-byte Folded Reload
	str	xzr, [x25]
	cbz	x8, .LBB7_20
.LBB7_2:                                // =>This Loop Header: Depth=1
                                        //     Child Loop BB7_4 Depth 2
                                        //       Child Loop BB7_5 Depth 3
	mov	x24, xzr
	mov	x25, xzr
	mov	x27, x21
	mov	x21, xzr
	b	.LBB7_4
.LBB7_3:                                //   in Loop: Header=BB7_4 Depth=2
	cbz	x27, .LBB7_1
.LBB7_4:                                //   Parent Loop BB7_2 Depth=1
                                        // =>  This Loop Header: Depth=2
                                        //       Child Loop BB7_5 Depth 3
	mov	x26, xzr
	str	x24, [sp, #8]                   // 8-byte Folded Spill
	add	x24, x24, #1
	mov	x8, x27
.LBB7_5:                                //   Parent Loop BB7_2 Depth=1
                                        //     Parent Loop BB7_4 Depth=2
                                        // =>    This Inner Loop Header: Depth=3
	ldr	x8, [x8]
	add	x26, x26, #1
	cbz	x8, .LBB7_8
// %bb.6:                               //   in Loop: Header=BB7_5 Depth=3
	cmp	x22, x26
	b.ne	.LBB7_5
// %bb.7:                               //   in Loop: Header=BB7_4 Depth=2
	mov	x26, x22
.LBB7_8:                                //   in Loop: Header=BB7_4 Depth=2
	mov	x9, x27
	mov	x28, x22
	mov	x23, x9
	mov	x27, x8
	cmp	x26, #0
	b.gt	.LBB7_13
	b	.LBB7_10
.LBB7_9:                                //   in Loop: Header=BB7_4 Depth=2
	mov	x21, x10
	mov	x25, x10
	mov	x23, x9
	mov	x27, x8
	cmp	x26, #0
	b.gt	.LBB7_13
.LBB7_10:                               //   in Loop: Header=BB7_4 Depth=2
	subs	x11, x28, #1
	b.lt	.LBB7_3
// %bb.11:                              //   in Loop: Header=BB7_4 Depth=2
	cbz	x27, .LBB7_3
// %bb.12:                              //   in Loop: Header=BB7_4 Depth=2
	cbz	x26, .LBB7_19
.LBB7_13:                               //   in Loop: Header=BB7_4 Depth=2
	cbz	x28, .LBB7_17
// %bb.14:                              //   in Loop: Header=BB7_4 Depth=2
	cbz	x27, .LBB7_17
// %bb.15:                              //   in Loop: Header=BB7_4 Depth=2
	ldr	x0, [x23, #8]
	ldr	x1, [x27, #8]
	mov	x2, x19
	blr	x20
	cmp	x0, #1
	b.lt	.LBB7_17
// %bb.16:                              //   in Loop: Header=BB7_4 Depth=2
	ldr	x8, [x27]
	sub	x28, x28, #1
	mov	x9, x23
	mov	x10, x27
	cbnz	x25, .LBB7_18
	b	.LBB7_9
.LBB7_17:                               //   in Loop: Header=BB7_4 Depth=2
	ldr	x9, [x23]
	sub	x26, x26, #1
	mov	x8, x27
	mov	x10, x23
	cbz	x25, .LBB7_9
.LBB7_18:                               //   in Loop: Header=BB7_4 Depth=2
	str	x10, [x25]
	mov	x23, x9
	mov	x27, x8
	mov	x25, x10
	cmp	x26, #0
	b.gt	.LBB7_13
	b	.LBB7_10
.LBB7_19:                               //   in Loop: Header=BB7_4 Depth=2
	ldr	x8, [x27]
	mov	x9, x23
	mov	x10, x27
	mov	x28, x11
	cbnz	x25, .LBB7_18
	b	.LBB7_9
.LBB7_20:
	mov	x0, x21
	.cfi_def_cfa wsp, 112
	ldp	x20, x19, [sp, #96]             // 16-byte Folded Reload
	ldp	x22, x21, [sp, #80]             // 16-byte Folded Reload
	ldp	x24, x23, [sp, #64]             // 16-byte Folded Reload
	ldp	x26, x25, [sp, #48]             // 16-byte Folded Reload
	ldp	x28, x27, [sp, #32]             // 16-byte Folded Reload
	ldp	x29, x30, [sp, #16]             // 16-byte Folded Reload
	add	sp, sp, #112
	.cfi_def_cfa_offset 0
	.cfi_restore w19
	.cfi_restore w20
	.cfi_restore w21
	.cfi_restore w22
	.cfi_restore w23
	.cfi_restore w24
	.cfi_restore w25
	.cfi_restore w26
	.cfi_restore w27
	.cfi_restore w28
	.cfi_restore w30
	.cfi_restore w29
	ret
.Lfunc_end7:
	.size	core_list_mergesort, .Lfunc_end7-core_list_mergesort
	.cfi_endproc
                                        // -- End function
	.globl	core_list_remove                // -- Begin function core_list_remove
	.p2align	2
	.type	core_list_remove,@function
core_list_remove:                       // @core_list_remove
	.cfi_startproc
// %bb.0:
	ldp	x8, x9, [x0]
	ldr	q0, [x8]
	str	q0, [x0]
	mov	x0, x8
	stp	xzr, x9, [x8]
	ret
.Lfunc_end8:
	.size	core_list_remove, .Lfunc_end8-core_list_remove
	.cfi_endproc
                                        // -- End function
	.globl	core_list_undo_remove           // -- Begin function core_list_undo_remove
	.p2align	2
	.type	core_list_undo_remove,@function
core_list_undo_remove:                  // @core_list_undo_remove
	.cfi_startproc
// %bb.0:
	ldr	q0, [x1]
	ldr	x8, [x0, #8]
	str	q0, [x0]
	stp	x0, x8, [x1]
	ret
.Lfunc_end9:
	.size	core_list_undo_remove, .Lfunc_end9-core_list_undo_remove
	.cfi_endproc
                                        // -- End function
	.globl	core_list_init                  // -- Begin function core_list_init
	.p2align	2
	.type	core_list_init,@function
core_list_init:                         // @core_list_init
	.cfi_startproc
// %bb.0:
	add	x9, x1, x0, lsl #4
	mov	w11, #32896                     // =0x8080
	cmp	x0, #3
	mov	x10, x9
	stp	xzr, x9, [x1]
	str	w11, [x10], #4
	add	x11, x1, #16
	b.lt	.LBB10_2
// %bb.1:
	str	x11, [x1]
	mov	x8, x11
	add	x11, x1, #32
	stp	xzr, x10, [x1, #16]
	add	x10, x9, #8
	mov	w12, #2147483647                // =0x7fffffff
	str	w12, [x9, #4]
	b	.LBB10_3
.LBB10_2:
	mov	x8, xzr
.LBB10_3:
	add	x13, x9, x0, lsl #2
	mov	x12, xzr
	mov	w14, #32767                     // =0x7fff
	b	.LBB10_5
.LBB10_4:                               //   in Loop: Header=BB10_5 Depth=1
	add	x12, x12, #1
	cmp	x0, x12
	b.eq	.LBB10_8
.LBB10_5:                               // =>This Inner Loop Header: Depth=1
	add	x15, x11, #16
	cmp	x15, x9
	b.hs	.LBB10_4
// %bb.6:                               //   in Loop: Header=BB10_5 Depth=1
	add	x16, x10, #4
	cmp	x16, x13
	b.hs	.LBB10_4
// %bb.7:                               //   in Loop: Header=BB10_5 Depth=1
	eor	w17, w12, w2
	and	w18, w12, #0x7
	str	x8, [x11]
	bfi	w18, w17, #3, #4
	str	x11, [x1]
	str	x10, [x11, #8]
	orr	w8, w18, w18, lsl #8
	strh	w14, [x10, #2]
	strh	w8, [x10]
	mov	x8, x11
	mov	x10, x16
	mov	x11, x15
	b	.LBB10_4
.LBB10_8:
	ldr	x10, [x8]
	cbz	x10, .LBB10_11
// %bb.9:
	mov	x9, #-3689348814741910324       // =0xcccccccccccccccc
	mov	w11, #1                         // =0x1
	movk	x9, #52429
	umulh	x9, x0, x9
	lsr	x9, x9, #2
.LBB10_10:                              // =>This Inner Loop Header: Depth=1
	add	x12, x11, #1
	eor	w13, w11, w2
	mov	x15, x10
	ubfiz	w14, w12, #8, #3
	and	w10, w13, #0x3fff
	cmp	x11, x9
	orr	w13, w14, w10
	ldr	x14, [x8, #8]
	ldr	x10, [x15]
	csel	w11, w11, w13, lo
	mov	x8, x15
	strh	w11, [x14, #2]
	mov	x11, x12
	cbnz	x10, .LBB10_10
.LBB10_11:
	mov	w8, #1                          // =0x1
	b	.LBB10_13
.LBB10_12:                              //   in Loop: Header=BB10_13 Depth=1
	lsl	x8, x8, #1
	str	xzr, [x11]
	cbz	x9, .LBB10_31
.LBB10_13:                              // =>This Loop Header: Depth=1
                                        //     Child Loop BB10_15 Depth 2
                                        //       Child Loop BB10_16 Depth 3
	mov	x10, xzr
	mov	x11, xzr
	mov	x13, x1
	mov	x1, xzr
	b	.LBB10_15
.LBB10_14:                              //   in Loop: Header=BB10_15 Depth=2
	cbz	x13, .LBB10_12
.LBB10_15:                              //   Parent Loop BB10_13 Depth=1
                                        // =>  This Loop Header: Depth=2
                                        //       Child Loop BB10_16 Depth 3
	mov	x12, xzr
	mov	x9, x10
	add	x10, x10, #1
	mov	x15, x13
.LBB10_16:                              //   Parent Loop BB10_13 Depth=1
                                        //     Parent Loop BB10_15 Depth=2
                                        // =>    This Inner Loop Header: Depth=3
	ldr	x15, [x15]
	add	x12, x12, #1
	cbz	x15, .LBB10_19
// %bb.17:                              //   in Loop: Header=BB10_16 Depth=3
	cmp	x8, x12
	b.ne	.LBB10_16
// %bb.18:                              //   in Loop: Header=BB10_15 Depth=2
	mov	x12, x8
.LBB10_19:                              //   in Loop: Header=BB10_15 Depth=2
	mov	x17, x13
	mov	x14, x8
	mov	x16, x17
	mov	x13, x15
	cmp	x12, #0
	b.gt	.LBB10_24
	b	.LBB10_21
.LBB10_20:                              //   in Loop: Header=BB10_15 Depth=2
	mov	x1, x18
	mov	x11, x18
	mov	x16, x17
	mov	x13, x15
	cmp	x12, #0
	b.gt	.LBB10_24
.LBB10_21:                              //   in Loop: Header=BB10_15 Depth=2
	subs	x0, x14, #1
	b.lt	.LBB10_14
// %bb.22:                              //   in Loop: Header=BB10_15 Depth=2
	cbz	x13, .LBB10_14
// %bb.23:                              //   in Loop: Header=BB10_15 Depth=2
	cbz	x12, .LBB10_30
.LBB10_24:                              //   in Loop: Header=BB10_15 Depth=2
	cbz	x14, .LBB10_28
// %bb.25:                              //   in Loop: Header=BB10_15 Depth=2
	cbz	x13, .LBB10_28
// %bb.26:                              //   in Loop: Header=BB10_15 Depth=2
	ldr	x15, [x16, #8]
	ldr	x18, [x13, #8]
	ldrb	w17, [x15, #1]
	strb	w17, [x15]
	ldrb	w17, [x18, #1]
	strb	w17, [x18]
	ldrsh	w17, [x18, #2]
	ldrsh	w15, [x15, #2]
	cmp	w15, w17
	b.le	.LBB10_28
// %bb.27:                              //   in Loop: Header=BB10_15 Depth=2
	ldr	x15, [x13]
	sub	x14, x14, #1
	mov	x17, x16
	mov	x18, x13
	cbnz	x11, .LBB10_29
	b	.LBB10_20
.LBB10_28:                              //   in Loop: Header=BB10_15 Depth=2
	ldr	x17, [x16]
	sub	x12, x12, #1
	mov	x15, x13
	mov	x18, x16
	cbz	x11, .LBB10_20
.LBB10_29:                              //   in Loop: Header=BB10_15 Depth=2
	str	x18, [x11]
	mov	x16, x17
	mov	x13, x15
	mov	x11, x18
	cmp	x12, #0
	b.gt	.LBB10_24
	b	.LBB10_21
.LBB10_30:                              //   in Loop: Header=BB10_15 Depth=2
	ldr	x15, [x13]
	mov	x17, x16
	mov	x18, x13
	mov	x14, x0
	cbnz	x11, .LBB10_29
	b	.LBB10_20
.LBB10_31:
	mov	x0, x1
	ret
.Lfunc_end10:
	.size	core_list_init, .Lfunc_end10-core_list_init
	.cfi_endproc
                                        // -- End function
	.globl	core_list_insert_new            // -- Begin function core_list_insert_new
	.p2align	2
	.type	core_list_insert_new,@function
core_list_insert_new:                   // @core_list_insert_new
	.cfi_startproc
// %bb.0:
	ldr	x8, [x2]
	add	x9, x8, #16
	cmp	x9, x4
	b.hs	.LBB11_3
// %bb.1:
	ldr	x10, [x3]
	add	x10, x10, #4
	cmp	x10, x5
	b.hs	.LBB11_3
// %bb.2:
	str	x9, [x2]
	ldr	x9, [x0]
	str	x9, [x8]
	str	x8, [x0]
	mov	x0, x8
	ldr	x9, [x3]
	str	x9, [x8, #8]
	add	x9, x9, #4
	str	x9, [x3]
	ldr	x9, [x8, #8]
	ldr	w10, [x1]
	str	w10, [x9]
	ret
.LBB11_3:
	mov	x0, xzr
	ret
.Lfunc_end11:
	.size	core_list_insert_new, .Lfunc_end11-core_list_insert_new
	.cfi_endproc
                                        // -- End function
	.ident	"clang version 18.1.5 (/home/muke/Programming/Memory_Renaming/llvm/clang 32112e08a9133b16e141f748a7a5c8376eb7cf5c)"
	.section	".note.GNU-stack","",@progbits
