//==-- AArch64StackRenamingLabeller.cpp - Replace dead defs w/ zero reg --==//
//
// Part of the LLVM Project, under the Apache License v2.0 with LLVM Exceptions.
// See https://llvm.org/LICENSE.txt for license information.
// SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception
//
//===----------------------------------------------------------------------===//
// Scan stack operations in a function to match accesses to the same locations
// and modify opcodes for automatic memory renaming by CPU frontend.
//===----------------------------------------------------------------------===//

#include "AArch64.h"
#include "AArch64RegisterInfo.h"
#include "AArch64Subtarget.h"
#include "AArch64InstrInfo.h"
#include "AArch64MachineFunctionInfo.h"
#include "AArch64Subtarget.h"
#include "MCTargetDesc/AArch64MCTargetDesc.h"
#include "llvm/ADT/Statistic.h"
#include "llvm/CodeGen/ISDOpcodes.h"
#include "llvm/CodeGen/MachineFunction.h"
#include "llvm/CodeGen/MachineFunctionPass.h"
#include "llvm/CodeGen/MachineInstr.h"
#include "llvm/CodeGen/MachineRegisterInfo.h"
#include "llvm/CodeGen/TargetInstrInfo.h"
#include "llvm/CodeGen/TargetSubtargetInfo.h"
#include "llvm/Support/Debug.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/IR/Metadata.h"
#include "llvm/IR/Instruction.h"
#include "llvm/CodeGen/MachineDominators.h"
using namespace llvm;

#define DEBUG_TYPE "aarch64-stack-renaming"

STATISTIC(NumOpcodesModified, "Number of load opcodes modified");

#define AARCH64_STACK_RENAMING_NAME "AArch64 Stack Renaming"

uint32_t BASE_ID = 1; //allows infinite IDs across functions

namespace {
class AArch64StackRenamingLabeller : public MachineFunctionPass {
private:
    const TargetRegisterInfo *TRI;
    const MachineRegisterInfo *MRI;
    const AArch64InstrInfo *TII;
    bool Changed;
    unsigned NumOpcodesModified;
    struct stackAccess {
        MachineInstr *inst;
        int frame_index_1 = 0;
        int frame_index_2 = 0;
    };
    std::vector<MachineInstr *> stackAccesses;
    MachineDominatorTree *MDT;

    void processMachineBasicBlock(MachineBasicBlock &MBB);
    void passStackStores(MachineBasicBlock &MBB);
    void passStackLoads(MachineBasicBlock &MBB);
public:
  static char ID; // Pass identification, replacement for typeid.
  AArch64StackRenamingLabeller() : MachineFunctionPass(ID) {
    initializeAArch64StackRenamingLabellerPass(
        *PassRegistry::getPassRegistry());
  }

  bool runOnMachineFunction(MachineFunction &F) override;

  StringRef getPassName() const override { return AARCH64_STACK_RENAMING_NAME; }

  void getAnalysisUsage(AnalysisUsage &AU) const override {
    AU.setPreservesCFG();
    MachineFunctionPass::getAnalysisUsage(AU);
  }
};
char AArch64StackRenamingLabeller::ID = 0;
} // end anonymous namespace

INITIALIZE_PASS(AArch64StackRenamingLabeller, "aarch64-stack-renaming",
                AARCH64_STACK_RENAMING_NAME, false, false)

// void AArch64StackRenamingLabeller::passStackStores(MachineBasicBlock &MBB) {
//     for (MachineInstr &MI: MBB){
//         for (auto MemOp: MI.memoperands()){
//             if (!MemOp->isStore()) continue;
//             const PseudoSourceValue *PSV = MemOp->getPseudoValue();
//             const FixedStackPseudoSourceValue *FSPSV;
//             if (!PSV) continue;
//             if (PSV->isStack() && MemOp->isLoad()) {
//                 errs() << "Non-fixed stack instruction: " << MI;
//                 errs() << "Offset: " << MemOp->getOffset() << "\n";
//             }
//             if (!(FSPSV=dyn_cast_or_null<FixedStackPseudoSourceValue>(PSV))) continue;
//             int frame_index = FSPSV->getFrameIndex();
//             if (slot_map.find(frame_index) != slot_map.end()) {
//                 //FIXME: this will insert a ldp twice when we rly only want it once
//                 slot_map[frame_index].push_back(&MI);
//                 // errs() << "Match: " << MI;
//                 // errs() << "With: " << *slot_map[frame_index][0];
//                 // MI.label=true;
//                 // slot_map[frame_index][0]->label = true;
//             }
//             else {
//                 slot_map[frame_index] = std::vector<MachineInstr *>();
//                 slot_map[frame_index].push_back(&MI);
//             }
//         }
//     }
// }

// void AArch64StackRenamingLabeller::passStackLoads(MachineBasicBlock &MBB) {
//     for (MachineInstr &MI: MBB){
//         for (auto MemOp: MI.memoperands()){
//             if (!MemOp->isLoad()) continue;
//             const PseudoSourceValue *PSV = MemOp->getPseudoValue();
//             const FixedStackPseudoSourceValue *FSPSV;
//             if (!PSV) continue;
//             if (!(FSPSV=dyn_cast_or_null<FixedStackPseudoSourceValue>(PSV))) continue;
//             int frame_index = FSPSV->getFrameIndex();
//             if (slot_map.find(frame_index) != slot_map.end()) {
//                 slot_map[frame_index].push_back(&MI);
//             }
//             else {
//                 if (MI.id) {
//                   errs() << "Warning! Found instruction with only partial mem operand match: " << MI;
//                   errs() << "Matching instruction: " << *id_map[MI.id];
//                   exit(1);
//                 }
//             }
//         }
//     }
// }

void AArch64StackRenamingLabeller::processMachineBasicBlock(
    MachineBasicBlock &MBB) {

    //its theoretically possible that two ldp's could share one stack slot but not another, and currently this would incorrectly give them a shared ID.
    //but catching this corner case is very tedious so going to assume it doesn't happen and fix it if it does later


    bool do_labels = true;
    bool do_ids = true;

    for (MachineInstr &MI: MBB){
        for (auto MemOp: MI.memoperands()){
            const PseudoSourceValue *PSV = MemOp->getPseudoValue();
            const FixedStackPseudoSourceValue *FSPSV;
            if (!PSV) continue;
            if (!(FSPSV=dyn_cast_or_null<FixedStackPseudoSourceValue>(PSV))) continue;
            if (FSPSV->getFrameIndex() < 0) { errs() << "Warning, found negative frame index: " << MI; continue; }
            stackAccesses.push_back(&MI);
            break;
        }
    }

    // unsigned id_counter = 1;
    //     unsigned id = BASE_ID*id_counter;
    // for (auto &MI_1: stackAccesses) {
    //     std::vector<MachineInstr *> matching_accesses;
    //     for (auto &MI_2: stackAccesses) {
    //         for (auto MemOp_1: MI_1->memoperands()) {
    //             int frame_index_1 =
    //             for (auto MemOp_2: MI_2->memoperands()) {
    //                 if ()
    //             }
    //         }
    //     }
    // }

  //for each store, check if a path can be found that 1. reaches the load and 2. doesn't encounter another store to the same slot. if it doesn't,
  //disregard the store. if indirect stack writes are a problem, add a condition to look for such stores along the path - only need to worry about ones
  //that could clobber.

    //Numopcodesmodified += 1;
}

bool AArch64StackRenamingLabeller::runOnMachineFunction(MachineFunction &MF) {

  if (skipFunction(MF.getFunction())){
    return false;
  }

  BASE_ID++;
  stackAccesses.clear();
  MDT = new MachineDominatorTree(MF);

  TRI = MF.getSubtarget().getRegisterInfo();
  TII = MF.getSubtarget<AArch64Subtarget>().getInstrInfo();
  MRI = &MF.getRegInfo();
  LLVM_DEBUG(dbgs() << "***** AArch64StackRenamingLabeller *****\n");
  errs() << MF.getName() << "\n";
  for (auto &MBB : MF)
    processMachineBasicBlock(MBB);
  errs() << "\n";

  delete MDT;
  return false; //changed
}

FunctionPass *llvm::createAArch64StackRenamingLabeller() {
  return new AArch64StackRenamingLabeller();
}
